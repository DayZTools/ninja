﻿namespace Ninja.Lib.Compression.Lzss;

public static class CrcUtil
{
    public static uint Calculate(Stream source)
    {
        var buffer = new byte[1024];
        uint crc = 0;

        source.Seek(0, SeekOrigin.Begin);

        while (source.Position != source.Length)
        {
            var read = source.Read(buffer, 0, buffer.Length);
            for (var i = 0; i < read; i++)
            {
                unchecked
                {
                    crc += buffer[i];
                }
            }
        }

        return crc;
    }
}
