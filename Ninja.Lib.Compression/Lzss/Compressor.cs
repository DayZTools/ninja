﻿namespace Ninja.Lib.Compression.Lzss;

public static class Compressor
{
    public static void Compress(Stream source, Stream target)
    {
        var dict = new CompressionBuffer();
        while (source.Position != source.Length)
        {
            var chunk = new CompressionChunk(source);
            chunk.Compose(dict);
            chunk.Flush(target);
        }
        var bw = new BinaryWriter(target);
        bw.Write(CrcUtil.Calculate(source));
    }
}
