﻿namespace Ninja.Lib.Compression.Lzss;

public struct SequenceInspection
{
    public int SourceBytes { get; set; }
    public int SequenceBytes { get; set; }
}
