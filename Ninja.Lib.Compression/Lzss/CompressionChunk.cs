﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Compression.Lzss;

public class CompressionChunk
{
    private int length;
    private byte format;
    private byte[] data;
    private byte[] next;
    private BinaryReader _source;

    public static byte chunks = 8;
    public static int minBytesToPack = 3;
    public static int maxChunkSize = 18;
    public static int maxOffsetToUseWhitespaces = CompressionBuffer.DefaultSize - maxChunkSize;

    public CompressionChunk(Stream source)
    {
        _source = new BinaryReader(source);
        data = new byte[maxChunkSize * 2];
        next = new byte[maxChunkSize];
    }

    public int Compose(CompressionBuffer dict)
    {
        var packedTotal = 0;

        for (byte i = 0; i < chunks && _source.BaseStream.Length != _source.BaseStream.Position; ++i)
        {
            var pos = _source.BaseStream.Position;
            var chunkSize = (int) Math.Min(maxChunkSize, _source.BaseStream.Length - pos);
            var packed = 0;
            if (chunkSize < minBytesToPack)
            {
                packed = ComposeUncompressed(i, dict);
            }
            else
            {
                packed = ComposeCompressed(i, chunkSize, dict);
            }

            packedTotal += packed;
            _source.BaseStream.Seek(packed, SeekOrigin.Current);
        }

        return packedTotal;
    }

    public int Flush(Stream target)
    {
        var bw = new BinaryWriter(target);
        bw.Write(format);
        bw.Write(data, 0, length);
        bw.Flush();
        return length + 1;
    }

    private int ComposeCompressed(byte chunk, int chunkSize, CompressionBuffer dict)
    {
        var bytesToCopy = 0;
        _source.Peek(next, 0, chunkSize);

        var intersect = dict.Intersect(next, chunkSize);

        var whitespace = _source.BaseStream.Position < maxOffsetToUseWhitespaces
            ? dict.CheckWhitespace(next, chunkSize)
            : 0;

        var sequence = dict.CheckSequence(next, chunkSize);

        if (intersect.Length >= minBytesToPack
            || whitespace >= minBytesToPack
            || sequence.SourceBytes >= minBytesToPack)
        {
            short pointer;
            if (intersect.Length >= whitespace && intersect.Length >= sequence.SourceBytes)
            {
                pointer = ComposePointer(dict.Fulfillment - intersect.Position, intersect.Length);
                bytesToCopy += intersect.Length;
            }
            else if (whitespace >= intersect.Length && whitespace >= sequence.SourceBytes)
            {
                pointer = ComposePointer((int) _source.BaseStream.Position + whitespace, whitespace);
                bytesToCopy += whitespace;
            }
            else
            {
                pointer = ComposePointer(sequence.SequenceBytes,  sequence.SourceBytes);
                bytesToCopy += sequence.SourceBytes;
            }
            dict.Add(_source, bytesToCopy);
            var c = BitConverter.GetBytes(pointer);
            data[length] = c[0];
            data[length + 1] = c[1];
            length += 2;
        }
        else
        {
            bytesToCopy = ComposeUncompressed(chunk, dict);
        }

        return bytesToCopy;
    }

    private int ComposeUncompressed(byte chunk, CompressionBuffer dict)
    {
        var bytesToCopy = 1;
        _source.Peek(out var b);
        data[length] = b;
        format += (byte) (1 << chunk);
        length += bytesToCopy;
        dict.Add(b);
        return bytesToCopy;
    }
    
    private short ComposePointer(int offset, int length)
    {
        unchecked
        {
            var vLength = (short)((length - minBytesToPack) << 8);
            var vOffset = (short)(((offset & 0x0F00) << 4) + (offset & 0x00FF));
            var r = (short)(vOffset + vLength);
            return r;
        }
       
    }
}
