﻿namespace Ninja.Lib.Compression.Lzss;

public class Decompressor
{
    public static uint Decompress(Stream input, out byte[] dst, int expectedSize, bool useSignedChecksum)
    {
        const int N = 4096;
        const int F = 18;
        const int THRESHOLD = 2;
        var buffer = new char[N + F - 1];
        dst = new byte[expectedSize];

        if (expectedSize <= 0) return 0;

        var startPos = input.Position;
        var bytesLeft = expectedSize;
        var iDst = 0;

        int c, csum = 0;
        var r = N - F;
        for (var i = 0; i < r; i++) buffer[i] = ' ';
        var flags = 0;
        while (bytesLeft > 0)
        {
            if (((flags >>= 1) & 256) == 0)
            {
                c = input.ReadByte();
                flags = c | 0xff00;
            }

            if ((flags & 1) != 0)
            {
                c = input.ReadByte();
                if (useSignedChecksum)
                    csum += (sbyte)c;
                else
                    csum += (byte)c;

                // save byte
                dst[iDst++] = (byte)c;
                bytesLeft--;
                // continue decompression
                buffer[r] = (char)c;
                r++;
                r &= (N - 1);
            }
            else
            {
                var i = input.ReadByte();
                var j = input.ReadByte();
                i |= (j & 0xf0) << 4;
                j &= 0x0f;
                j += THRESHOLD;

                if (j + 1 > bytesLeft)
                {
                    throw new ArgumentException("LZSS overflow");
                }

                var ii = r - i;
                var jj = j + ii;
                for (; ii <= jj; ii++)
                {
                    c = (byte)buffer[ii & (N - 1)];
                    if (useSignedChecksum)
                        csum += (sbyte)c;
                    else
                        csum += (byte)c;

                    // save byte
                    dst[iDst++] = (byte)c;
                    bytesLeft--;
                    // continue decompression
                    buffer[r] = (char)c;
                    r++;
                    r &= (N - 1);
                }
            }
        }

        var csData = new byte[4];
        input.Read(csData, 0, 4);
        var csr = BitConverter.ToInt32(csData, 0);

        if (csr != csum)
        {
            throw new ArgumentException("Checksum mismatch");
        }

        return (uint)(input.Position - startPos);
    }
}
