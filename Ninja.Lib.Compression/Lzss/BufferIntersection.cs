﻿namespace Ninja.Lib.Compression.Lzss;

public struct BufferIntersection
{
    public int Position { get; set; }
    public int Length { get; set; }

    public BufferIntersection()
    {
        Position = -1;
        Length = 0;
    }
}
