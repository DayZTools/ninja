﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Compression.Lzss;

public class CompressionBuffer
{
    private readonly int _size;
    private readonly byte[] _data;

    public const int DefaultSize = 0b0000111111111111;

    public int Fulfillment { get; private set; }

    public CompressionBuffer(int size = DefaultSize)
    {
        this._size = size;
        Fulfillment = 0;
        _data = new byte[size];
    }

    public BufferIntersection Intersect(IReadOnlyList<byte> buffer, int length)
    {
        var intersection = new BufferIntersection();
        if (length <= 0 || Fulfillment <= 0) return intersection;
        var offset = 0;
        var next = new BufferIntersection();
        while (true)
        {
            IntersectBufferAtOffset(buffer, length, offset, ref next);
            if (next.Position >= 0 && intersection.Length < next.Length)
            {
                intersection = next;
            }

            if (next.Position >= 0 && next.Position <= Fulfillment - 1)
            {
                offset = next.Position + 1;
            }
            else
            {
                break;
            }
        }

        return intersection;
    }

    public int CheckWhitespace(IReadOnlyList<byte> buffer, int length)
    {
        var count = 0;
        for (var i = 0; i < length; i++)
        {
            if (buffer[i] == 0x20)
            {
                count++;
            }
            else
            {
                break;
            }
        }

        return count;
    }

    public SequenceInspection CheckSequence(IReadOnlyList<byte> buffer, int length)
    {
        var result = new SequenceInspection();
        var sequence = new SequenceInspection();

        var maxSourceBytes = Math.Min(Fulfillment, length);

        for (var i = 1; i < maxSourceBytes; i++)
        {
            CheckSequenceImpl(buffer, length, i, ref sequence);
            if (sequence.SourceBytes > result.SourceBytes)
            {
                result = sequence;
            }
        }

        return result;
    }

    public void Add(BinaryReader source, int length)
    {
        if (Fulfillment + length > _size)
        {
            //shift the buffer contents left until there is enough space for the new bunch of bytes
            var bytesToMoveCount = _size - length;
            Array.Copy(_data, Fulfillment - bytesToMoveCount, _data, 0, bytesToMoveCount);

            source.Peek(_data, bytesToMoveCount, length);
            Fulfillment = _size;
        }
        else
        {
            //add some bytes onto the free space of the buffer
            source.Peek(_data, Fulfillment, length);
            Fulfillment += length;
        }
    }

    public void Add(byte b)
    {
        const int length = 1;
        if (Fulfillment + length > _size)
        {
            //shift the buffer contents left until there is enough space for the new bunch of bytes
            var bytesToMoveCount = _size - length;
            Array.Copy(_data, Fulfillment - bytesToMoveCount, _data, 0, bytesToMoveCount);

            _data[bytesToMoveCount] = b;
            Fulfillment = _size;
        }
        else
        {
            //add some bytes onto the free space of the buffer
            _data[Fulfillment] = b;
            Fulfillment += length;
        }
    }


    private void IntersectBufferAtOffset(IReadOnlyList<byte> buffer, int bLength, int offset,
        ref BufferIntersection intersection)
    {
        intersection.Position = Array.IndexOf(_data, buffer[0], offset, _data.Length - offset);
        intersection.Length = 0;

        if (intersection.Position < 0 || intersection.Position >= Fulfillment) return;

        intersection.Length += 1;
        var dataIndex = intersection.Position + 1;
        for (var bufIndex = 1; bufIndex < bLength && dataIndex < Fulfillment; bufIndex++, dataIndex++)
        {
            if (_data[dataIndex] == buffer[bufIndex])
                intersection.Length++;
            else break;
        }
    }

    private void CheckSequenceImpl(IReadOnlyList<byte> buffer, int length, int sequenceBytes,
        ref SequenceInspection sequence)
    {
        sequence.SourceBytes = 0;
        sequence.SequenceBytes = sequenceBytes;

        while (sequence.SourceBytes < length)
        {
            for (var i = Fulfillment - sequenceBytes; i < Fulfillment && sequence.SourceBytes < length; i++)
            {
                if (buffer[sequence.SourceBytes] == _data[i])
                {
                    sequence.SourceBytes++;
                }
                else
                {
                    return;
                }
            }
        }
    }
}
