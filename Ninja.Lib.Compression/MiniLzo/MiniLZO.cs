﻿using System.Runtime.InteropServices;

namespace Ninja.Lib.Compression.MiniLzo;

public class MiniLZO
{
    [DllImport("lzo")]
    private static extern unsafe int lzo1x_decompress(
        byte* src,
        uint src_len,
        byte* dst,
        ref uint dst_len,
        IntPtr wrkmem);
    
    [DllImport("lzo")]
    private static extern unsafe int lzo1x_decompress_safe(
        byte* src,
        uint src_len,
        byte* dst,
        ref uint dst_len,
        IntPtr wrkmem);

    [DllImport("lzo")]
    private static extern unsafe int lzo1x_1_compress(
        byte* src,
        uint src_len,
        byte* dst,
        ref uint dst_len,
        byte *wrkmem);

    public static uint Decompress(byte[] @in, ref byte[] @out)
    {
        uint outSz = 0;
        unsafe
        {
            fixed (byte* input = &@in[0])
            fixed (byte* output = &@out[0])
            {
                var r = lzo1x_decompress(input, (uint)@in.Length, output, ref outSz, IntPtr.Zero);
                if (r != 0)
                {
                    throw new Exception("Failed to decompress data");
                }
            }
        }

        return outSz;
    }

    public static byte[] Compress(byte[] @in)
    {
        var dst = new byte[@in.Length];
        uint outSz = 0;
        unsafe
        {
            fixed (byte* input = &@in[0])
            fixed (byte* output = &dst[0])
            fixed (byte* wrkmem = new byte[IntPtr.Size * 16384])
            {
                var r = lzo1x_1_compress(input, (uint)@in.Length, output, ref outSz, wrkmem);
                if (r != 0)
                {
                    throw new Exception("Failed to compress data");
                }
            }
        }

        Array.Resize(ref dst, (int) outSz);

        return dst;
    }
}
