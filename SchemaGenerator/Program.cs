﻿using Newtonsoft.Json.Schema.Generation;
using NinjaMake.Models;

var generator = new JSchemaGenerator();
generator.GenerationProviders.Add(new StringEnumGenerationProvider());

var schema = generator.Generate(typeof(Config));

Console.WriteLine(schema.ToString());
