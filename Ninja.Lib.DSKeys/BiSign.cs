﻿using System.Text;
using Ninja.Lib.Utils;

namespace Ninja.Lib.DSKeys;

public readonly struct BiSign
{
    public string Name { get; }
    public byte[] Signature { get; }
    public string Type { get; }

    public int Version { get; }
    public int KeyLengthBits { get; }
    public int KeyLength => KeyLengthBits / 8;
    public byte[] Exponent { get; }

    public byte[] Modulus { get; }
    public byte[] Sig1 { get; }
    public byte[] Sig2 { get; }
    public byte[] Sig3 { get; }

    internal BiSign(string name, int keyLengthBits, int version, byte[] signature,
        string type, byte[] exponent, byte[] modulus, byte[] sig1, byte[] sig2, byte[] sig3)
    {
        Name = name;
        Modulus = modulus;
        Exponent = exponent;
        KeyLengthBits = keyLengthBits;
        Version = version;
        Signature = signature;
        Type = type;
        Sig1 = sig1;
        Sig2 = sig2;
        Sig3 = sig3;
    }

    public static BiSign ReadKey(string file)
    {
        using var inStream = File.OpenRead(file);
        return ReadKey(inStream);
    }

    public static BiSign ReadKey(Stream inStream)
    {
        var br = new BinaryReader(inStream);

        var name = br.ReadCString();
        /*var length =*/ br.ReadInt32();
        var signature = br.ReadBytes(8);
        var type = Encoding.UTF8.GetString(br.ReadBytes(4));
        var keyLengthBits = br.ReadInt32();
        var exponent = br.ReadBytes(4).Reverse().ToArray();
        var modulus = br.ReadBytes(keyLengthBits / 8).Reverse().ToArray();

        var sig1Len = br.ReadInt32();
        var sig1 = br.ReadBytes(sig1Len).Reverse().ToArray();

        var version = br.ReadInt32();
        var sig2Len = br.ReadInt32();
        var sig2 = br.ReadBytes(sig2Len).Reverse().ToArray();

        var sig3Len = br.ReadInt32();
        var sig3 = br.ReadBytes(sig3Len).Reverse().ToArray();
        return new BiSign(name, keyLengthBits, version, signature, type, exponent, modulus, sig1, sig2, sig3);
    }

    public void WriteKey(string outPath)
    {
        using var outStream = File.Create(outPath);
        var bw = new BinaryWriter(outStream);

        bw.WriteCString(Name);
        var headerLen = 4 + 4 + Signature.Length + Type.Length;
        var len = KeyLength + headerLen;
        bw.Write(len);
        bw.Write(Signature);
        bw.Write(Encoding.ASCII.GetBytes(Type)); // usually RSA1
        bw.Write(KeyLengthBits);

        bw.Write(Exponent.Reverse().ToArray());
        bw.Write(Modulus.Reverse().ToArray());

        bw.Write(Sig1.Length);
        bw.Write(Sig1.Reverse().ToArray());

        bw.Write(Version);

        bw.Write(Sig2.Length);
        bw.Write(Sig2.Reverse().ToArray());

        bw.Write(Sig3.Length);
        bw.Write(Sig3.Reverse().ToArray());
    }
}
