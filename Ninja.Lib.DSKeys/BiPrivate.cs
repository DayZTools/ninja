﻿using System.Security.Cryptography;
using System.Text;
using Ninja.Lib.Utils;

namespace Ninja.Lib.DSKeys;

public readonly struct BiPrivate
{
    public string Name { get; }
    public byte[] Signature { get; }
    public string Type { get; }
    public int KeySizeInBits { get; }
    public int KeySize => KeySizeInBits / 8;
    public byte[] Exponent { get; }

    public byte[] Modulus { get; }
    public byte[] P { get; }
    public byte[] Q { get; }
    public byte[] DP { get; }
    public byte[] DQ { get; }

    public byte[] InverseQ { get; }
    public byte[] D { get; }

    internal BiPrivate(string name, int keySizeInBits, byte[] signature, string type, byte[] exponent, byte[] modulus,
        byte[] p, byte[] q, byte[] dp, byte[] dq, byte[] inverseQ, byte[] d) : this()
    {
        Name = name;
        KeySizeInBits = keySizeInBits;
        Signature = signature;
        Type = type;
        Exponent = exponent;
        Modulus = modulus;
        P = p;
        Q = q;
        DP = dp;
        DQ = dq;
        InverseQ = inverseQ;
        D = d;
    }


    public static BiPrivate ReadKey(string file)
    {
        using var fileStream = File.OpenRead(file);
        return ReadKey(fileStream);
    }

    public static BiPrivate ReadKey(Stream inStream)
    {
        var br = new BinaryReader(inStream);

        var name = br.ReadCString();

        var length = br.ReadInt32();

        var signature = br.ReadBytes(8);
        var type = Encoding.UTF8.GetString(br.ReadBytes(4));

        var keySizeInBits = br.ReadInt32();
        var keySize = keySizeInBits / 8;
        var exponent = br.ReadBytes(4).Reverse().ToArray();
        var modulus = br.ReadBytes(keySize).Reverse().ToArray();
        var p = br.ReadBytes(keySize / 2).Reverse().ToArray();
        var q = br.ReadBytes(keySize / 2).Reverse().ToArray();
        var dp = br.ReadBytes(keySize / 2).Reverse().ToArray();
        var dq = br.ReadBytes(keySize / 2).Reverse().ToArray();
        var inverseQ = br.ReadBytes(keySize / 2).Reverse().ToArray();
        var d = br.ReadBytes(keySizeInBits);

        return new BiPrivate(name, keySizeInBits, signature, type, exponent, modulus, p, q, dp, dq, inverseQ, d);
    }

    public void WriteKey(string privateKeyPath)
    {
        using var outStream = File.Create(privateKeyPath);
        WriteKey(outStream);
    }

    public void WriteKey(Stream outStream)
    {
        var bw = new BinaryWriter(outStream);
        bw.WriteCString(Name);

        var length = Exponent.Length + Modulus.Length + P.Length + Q.Length + DP.Length + DQ.Length + InverseQ.Length +
                     D.Length;
        length += Signature.Length + Type.Length;
        length += 4; // self

        bw.Write(length);

        bw.Write(Signature);
        bw.Write(Encoding.ASCII.GetBytes(Type));

        bw.Write(KeySizeInBits);
        bw.Write(Exponent.Reverse().ToArray());
        bw.Write(Modulus.Reverse().ToArray());
        bw.Write(P.Reverse().ToArray());
        bw.Write(Q.Reverse().ToArray());
        bw.Write(DP.Reverse().ToArray());
        bw.Write(DQ.Reverse().ToArray());
        bw.Write(InverseQ.Reverse().ToArray());
        bw.Write(D.Reverse().ToArray());
    }

    public static BiPrivate Generate(string name, int keySizeInBits = 1024)
    {
        using var rsa = RSA.Create(keySizeInBits);
        var rsaParameters = rsa.ExportParameters(true);

        var exp = new List<byte> { 0 };
        exp.AddRange(rsaParameters.Exponent!);

        var key = new BiPrivate(name, keySizeInBits,
            new byte[] { 0x07, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00 }, "RSA2",
            exp.ToArray(),
            rsaParameters.Modulus!, rsaParameters.P!, rsaParameters.Q!, rsaParameters.DP!,
            rsaParameters.DQ!, rsaParameters.InverseQ!, rsaParameters.D!);

        return key;
    }

    public RSAParameters GetRSAParameters()
    {
        return new RSAParameters
        {
            Modulus = Modulus,
            D = D,
            P = P,
            Q = Q,
            DP = DP,
            DQ = DQ,
            InverseQ = InverseQ,
            Exponent = Exponent
        };
    }
}
