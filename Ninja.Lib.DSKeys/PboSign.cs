using System.Security.Cryptography;
using System.Text;
using Ninja.Lib.Pbo;

namespace Ninja.Lib.DSKeys;

public class PboSign
{
    public Pbo.Pbo File { get; init; }
    public PboClient Client { get; init; }
    
    public PboSign(Pbo.Pbo file, PboClient client)
    {
        File = file;
        Client = client;
    }


    private byte[]? GetPrefix()
    {
        var prefixStr = File.Properties
            .Where(x => x.Key == "prefix")
            .Select(x => x.Value)
            .FirstOrDefault();
        if (prefixStr != null && prefixStr[^1] != '\\')
        {
            prefixStr += '\\';
        }

        byte[]? prefix = null;
        if (prefixStr != null)
        {
            prefix = Encoding.ASCII.GetBytes(prefixStr);
        }

        return prefix;
    }
    
    private byte[] GenerateNamesHash()
    {
        using var sha = SHA1.Create();
        sha.Initialize();

        var fileNames = File.Files
            .Select(file => file.FileName.ToLower())
            .OrderBy(x => x)
            .Aggregate("", (current, fileName) => current + fileName)
            .ToArray();

        return sha.ComputeHash(Encoding.ASCII.GetBytes(fileNames));
    }

    private byte[] GenerateFilesHash(bool forceV2)
    {
        var tempFiles = new List<byte>();
        var hasFiles = false;

        List<string> exclLsit;

        //if (!forceV2)
        {
            exclLsit = new List<string>
            {
                "p3d", "paa", "tga", "rvmat", "lip", "ogg", "wss", "png", "jpg",
                "rtm", "pac", "fxy", "wrp", "nm", "xob", "anm", "edds", "emat",
                "ptc", "inc", "bikb", "ext", "hpp", "cfg", "h", "cpp", "bin"
            };
        }

        foreach (var file in File.Files)
        {
            var fileName = file.FileName;

            var ext = fileName[(fileName.LastIndexOf('.') + 1)..];
            if (exclLsit.Contains(ext))
                continue;


            hasFiles = true;
            var data = Client.Reader!.ExtractFileData(file, false);
            tempFiles.AddRange(data);
        }

        byte[] filehash;
        using var sha = SHA1.Create();
        sha.Initialize();

        if (!hasFiles)
        {
            if (forceV2)
                filehash = sha.ComputeHash(Encoding.ASCII.GetBytes("nothing"));
            else
                filehash = sha.ComputeHash(Encoding.ASCII.GetBytes("gnihton"));
            return filehash;
        }

        filehash = sha.ComputeHash(tempFiles.ToArray());
        return filehash;
    }

    private byte[] GenerateHash(IEnumerable<byte> hash1, IEnumerable<byte> hash2, IEnumerable<byte>? hash3)
    {
        var hashTemp = new List<byte>();
        hashTemp.AddRange(hash1);
        hashTemp.AddRange(hash2);
        if (hash3 != null)
        {
            hashTemp.AddRange(hash3);
        }

        using var sha = SHA1.Create();
        sha.Initialize();
        return sha.ComputeHash(hashTemp.ToArray());
    }

    private void PrepareHashes(bool forceV2, out byte[] hash1, out byte[] hash2, out byte[] hash3)
    {
        var prefix = GetPrefix();
        var nameHash = GenerateNamesHash();
        var fileHash = GenerateFilesHash(forceV2);

        hash1 = File.Checksum;
        hash2 = GenerateHash(hash1, nameHash, prefix);
        hash3 = GenerateHash(fileHash, nameHash, prefix);
    }
    
    public BiSign Sign(BiPrivate privateKey, bool forceV2 = false)
    {
        PrepareHashes(forceV2, out var hash1, out var hash2, out var hash3);

        using var rsa = RSA.Create(privateKey.KeySizeInBits);
        var rsaParams = privateKey.GetRSAParameters();
        rsa.ImportParameters(rsaParams);

        return new BiSign(privateKey.Name,
            privateKey.KeySizeInBits, forceV2 ? 2 : 3,
            new byte[] { 0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00 }, 
            "RSA1",
            rsaParams.Exponent!,
            rsaParams.Modulus!,
            rsa.SignHash(hash1, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1), rsa.SignHash(hash2, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1), rsa.SignHash(hash3, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1));
    }

    public bool CheckSign(BiPublic pubKey, BiSign sign)
    {
        PrepareHashes(sign.Version == 2, out var hash1, out var hash2, out var hash3);
        
        using var rsa = RSA.Create(pubKey.KeyLengthBits);
        rsa.ImportParameters(pubKey.GetRsaParameters());

        var b1 = rsa.VerifyHash(hash1, sign.Sig1, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
        var b2 = rsa.VerifyHash(hash2, sign.Sig2, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
        var b3 = rsa.VerifyHash(hash3, sign.Sig3, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
        return b1 && b2 && b3;
    }
}
