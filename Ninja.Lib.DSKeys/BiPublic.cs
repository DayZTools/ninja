﻿using System.Security.Cryptography;
using System.Text;
using Ninja.Lib.Utils;

namespace Ninja.Lib.DSKeys;

public readonly struct BiPublic
{
    public string Name { get; }
    public byte[] Signature { get; }
    public string Type { get; }
    public int KeyLengthBits { get; }
    public int KeyLength => KeyLengthBits / 8;
    public byte[] Exponent { get; }

    public byte[] Modulus { get; }

    internal BiPublic(string name, int keyLengthBits, byte[] signature, string type, byte[] exponent, byte[] modulus)
    {
        Name = name;
        Signature = signature;
        Type = type;
        KeyLengthBits = keyLengthBits;
        Exponent = exponent;
        Modulus = modulus;
    }

    public static BiPublic ReadKey(string file)
    {
        using var inStream = File.OpenRead(file);
        return ReadKey(inStream);
    }

    public static BiPublic ReadKey(Stream inStream)
    {
        var br = new BinaryReader(inStream);


        var name = br.ReadCString();
        var length = br.ReadInt32();
        var signature = br.ReadBytes(8);
        var type = Encoding.UTF8.GetString(br.ReadBytes(4));
        var keyLengthBits = br.ReadInt32();
        var exponent = br.ReadBytes(4).Reverse().ToArray();
        var modulus = br.ReadBytes(keyLengthBits / 8).Reverse().ToArray();
        return new BiPublic(name, keyLengthBits, signature, type, exponent, modulus);
    }

    public void WriteKey(string outPath)
    {
        using var outStream = File.Create(outPath);
        WriteKey(outStream);
    }

    public void WriteKey(Stream outStream)
    {
        var br = new BinaryWriter(outStream);
        br.WriteCString(Name);
        var headerLen = 4 + 4 + Signature.Length + Type.Length; // this length + KeyLength + Signature (8) + "RSA1"
        var len = KeyLength + headerLen;
        br.Write(len);
        br.Write(Signature);
        br.Write(Encoding.ASCII.GetBytes(Type)); // usually RSA1
        br.Write(KeyLengthBits);

        br.Write(Exponent.Reverse().ToArray());
        br.Write(Modulus.Reverse().ToArray());
    }

    public static BiPublic Generate(BiPrivate pkey)
    {
        var rsaParams = pkey.GetRSAParameters();
        var pub = new BiPublic(pkey.Name,
            pkey.KeySizeInBits,
            new byte[] { 0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00 },
            "RSA1", rsaParams.Exponent!, rsaParams.Modulus!);
        return pub;
    }

    public RSAParameters GetRsaParameters()
    {
        return new RSAParameters
        {
            Modulus = Modulus,
            Exponent = Exponent
        };
    }
}
