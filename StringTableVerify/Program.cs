﻿using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;

var file = File.OpenRead("P:\\languagecore\\stringtable.csv");
var reader = new StreamReader(file);

using var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
{
    IgnoreBlankLines = true
});

var tClass = typeof(Translation);
foreach (var translation in csv.GetRecords<Translation>())
{
    foreach (var property in tClass.GetProperties())
    {
        var val = property.GetValue(translation) as string;
        if (string.IsNullOrEmpty(val))
        {
            Console.WriteLine($"value is empty for translation {translation.Language}:{property.Name}");
        }
    }
}

public class Translation
{
    [Index(0)]
    public string Language { get; set; }
    [Index(1)]
    public string Original { get; set; }
    [Index(2)]
    public string English { get; set; }
    [Index(3)]
    public string Czech { get; set; }
    [Index(4)]
    public string German { get; set; }
    [Index(5)]
    public string Russian { get; set; }
    [Index(6)]
    public string Polish { get; set; }
    [Index(7)]
    public string Hungarian { get; set; }
    [Index(8)]
    public string Italian { get; set; }
    [Index(9)]
    public string Spanish { get; set; }
    [Index(10)]
    public string French { get; set; }
    [Index(11)]
    public string Chinese { get; set; }
    [Index(12)]
    public string Japanese { get; set; }
    [Index(13)]
    public string Portuguese { get; set; }
    [Index(14)]
    public string Chinesesimp { get; set; }
}
