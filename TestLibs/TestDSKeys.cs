using System.Linq;
using Ninja.Lib.DSKeys;
using NUnit.Framework;

namespace TestLibs;

public class TestDSKeys
{
    private BiPrivate _private;
    [SetUp]
    public void Setup()
    {
        _private = BiPrivate.Generate("Test Key");
    }

    [Test]
    public void CreateBiPublic()
    {
        var biKey = BiPublic.Generate(_private);
        Assert.IsTrue(biKey.Name == "Test Key");
        Assert.IsTrue(biKey.Modulus.SequenceEqual(_private.Modulus));
        Assert.IsTrue(biKey.Exponent.SequenceEqual(_private.Exponent));
    }
}
