﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Ninja.Lib.Compression.Lzss;

namespace TestLibs;

public class TestLZSS
{
    private byte[] demoData;
    [SetUp]
    public void Setup()
    {
        demoData = new byte[65535];
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        var randNum = new Random();
        for (var i = 0; i < demoData.Length; i++)
        {
            demoData[i] = (byte) chars[randNum.Next(chars.Length)];
        }
    }
    
    [Test]
    public void BasicLZSSTest()
    {
        var dict = new CompressionBuffer();
        using var source = new MemoryStream(demoData);
        using var target = new MemoryStream();
        Compressor.Compress(source, target);
        target.Seek(0, SeekOrigin.Begin);

        Decompressor.Decompress(target, out var result, demoData.Length, false);
        Assert.IsTrue(result.SequenceEqual(demoData));
    }
}
