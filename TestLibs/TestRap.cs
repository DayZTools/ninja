﻿using System.IO;
using System.Linq;
using System.Text;
using Ninja.Lib.Cpp;
using Ninja.Lib.Cpp.Entries;
using NUnit.Framework;

namespace TestLibs;

public class TestRap
{
    private readonly string payload = @"
class CfgPatches {
	class TestMod 
	{

	};
};
class EmptyClass {

};
class Another
{
	SuperValue = ""XXXX"";
    class SubClass {
        SuperValue = ""YYYY"";
    };
};
class Another2
{
    SuperValue = ""ZZZZ"";
};
MyVal = ""ABCD"";
NegValue = -0.005;

class Flashlight;

class Test: Flashlight
{
    displayName=""ABCD АБВГ"";

};
";

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void TestCPP()
    {
        var bytes = Encoding.UTF8.GetBytes(payload);
        var inStream = new MemoryStream(bytes);
        var outStream = new MemoryStream();
        var bw = new BinaryWriter(outStream);

        var parsedCpp = Rapify.CreateRapFile(inStream);

        Assert.IsTrue(parsedCpp.EntryPoint.Entries.Count == 8);
        
        var parsedVar = parsedCpp.EntryPoint.Entries.FirstOrDefault(x => x.Name == "MyVal") as Variable;
        Assert.IsNotNull(parsedVar);
        var parsedString = parsedVar!.Value as string;
        Assert.IsNotNull(parsedString);
        Assert.IsTrue(parsedString == "ABCD");
        
        var parsedVar2 =  parsedCpp.EntryPoint.Entries.FirstOrDefault(x => x.Name == "NegValue") as Variable;
        Assert.IsNotNull(parsedVar2);
        Assert.IsTrue(parsedVar2!.Value is float);
        var parsedFloat = (float) parsedVar2!.Value!;

        Assert.IsFalse((0.005f + parsedFloat) > 0.001);
        

        parsedCpp.Write(bw);

        var cppStream = new MemoryStream();
        outStream.Position = 0;

        var rapFile = new Ninja.Lib.Cpp.Reader.RapFile();
        var br = new BinaryReader(outStream);
        rapFile.Read(br);

        Assert.IsTrue(rapFile.EntryPoint.Entries.Count == 8);
        var readedVar = rapFile.EntryPoint.Entries.FirstOrDefault(x => x.Name == "MyVal") as Variable;
        Assert.IsNotNull(readedVar);
        var readedString = readedVar!.Value as string;
        Assert.IsNotNull(readedString);
        Assert.IsTrue(readedString == "ABCD");

        rapFile.SaveTo(cppStream);
        Assert.Pass();
    }
    
    [Test]
    public void TestComplexInheritance()
    {
        var complexPayload = @"
class CfgPatches
{
	class DZ_Data
	{
		requiredAddons[] = {};
        units[] = {};
        weapons[] = {};
        worlds[] = {};
    };
};

class CfgWorlds
{
    class Default
    {
        class Weather
        {
            class Overcast {
                class Weather1;
            };
        };
    };
    class CAWorld: Default
	{
        class Weather: Weather
		{
            class Overcast: Overcast
			{
				class Weather1: Weather1
				{
                };
            };
        };
    };
};";
        var bytes = Encoding.UTF8.GetBytes(complexPayload);
        var inStream = new MemoryStream(bytes);
        var outStream = new MemoryStream();
        var bw = new BinaryWriter(outStream);

        var parsedCpp = Rapify.CreateRapFile(inStream);
        var ep = parsedCpp.EntryPoint;
        Assert.IsNotEmpty(ep.Entries);
        var c = ep.Entries[1] as Class;
        Assert.IsNotNull(c);
        Assert.IsTrue(c.Entries.Count == 2);
        Assert.AreEqual(c.Entries[0].Name, "Default");
        Assert.AreEqual(c.Entries[1].Name, "CAWorld");

        var weather1 = (((c.Entries[1] as Class)?.Entries.First() as Class)?.Entries.First() as Class)?.Entries[0] as Class;
        Assert.IsNotNull(weather1);
        Assert.AreEqual(weather1.Name, "Weather1");
        Assert.AreEqual(weather1.Inherited, "Weather1");

        Assert.Pass();
    }
    [Test]
    public void TestComplexInheritance2()
    {
        var complexPayload = @"
class CfgPatches
{
	class DZ_Data
	{
		requiredAddons[] = {};
        units[] = {};
        weapons[] = {};
        worlds[] = {};
    };
};
class Mode_Single;
class cfgWeapons {
	class Shotgun_Base;
	class Izh43Shotgun_Base: Shotgun_Base
	{
        class Single: Mode_Single
		{
            reloadTime=1;
        };
    };
};";
        var bytes = Encoding.UTF8.GetBytes(complexPayload);
        var inStream = new MemoryStream(bytes);
        var outStream = new MemoryStream();
        var bw = new BinaryWriter(outStream);

        var parsedCpp = Rapify.CreateRapFile(inStream);
        
        Assert.Pass();
    }
}
