﻿namespace Ninja.Lib.ANM;

public class BoneHead
{
    public string Name { get; set; }

    public float TranslationBias { get; set; }

    public float TranslationMultiplier { get; set; }

    public float RotationBias { get; set; }

    public float RotationMultiplier { get; set; }

    public float ScaleBias { get; set; }

    public float ScaleMultiplier { get; set; }

    public ushort NumFrames { get; set; } = 0;

    public ushort TranslationFrameCount { get; set; } = 0;

    public ushort RotationFrameCount { get; set; } = 0;

    public ushort ScaleFrameCount { get; set; } = 0;

    public short Flags { get; set; } = 0;
}

public class BoneAnimation
{
    public ushort FrameCount { get; set; }
    public Dictionary<ushort, Vector3>? Translations { get; set; }
    public Dictionary<ushort, Vector3>? Scales { get; set; }
    public Dictionary<ushort, Quaternion>? Rotations { get; set; }
}
