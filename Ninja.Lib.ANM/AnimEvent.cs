﻿namespace Ninja.Lib.ANM;

public class AnimEvent
{
    public int Frame { get; set; }
    public string Name { get; set; }
    public string UnkValue1 { get; set; }
    public int UnkValue2 { get; set; }
}
