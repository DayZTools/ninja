﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.ANM.Entity;

public class Entity
{
    public const int DefaultTagLength = 4;
    public string Tag { get; private set; }
    protected int TagLen = DefaultTagLength;
    protected string ExpectedTag { get; init; }
    public int EntrySize { get; private set; }

    protected long DataOffset { get; private set; }

    public void Read(BinaryReader br)
    {
        if (br.BaseStream.Position >= br.BaseStream.Length) return;
        Tag = br.ReadRawString(TagLen);
        EntrySize = (int) Byteswap(br.ReadUInt32());
        DataOffset = br.BaseStream.Position;

        ValidateTag();
        
        ReadData(br);
    }

    protected virtual void ValidateTag()
    {
        if (ExpectedTag != Tag)
        {
            throw new InvalidOperationException("Got invalid tag" + Tag + " but expected " + ExpectedTag);
        }
    }

    protected virtual void ReadData(BinaryReader br)
    {
        br.BaseStream.Position += EntrySize;
    }

    public void Write(BinaryWriter bw)
    {
        bw.WriteRawString(ExpectedTag, TagLen);
        bw.Write(0); // will-be overwritten
        DataOffset = bw.BaseStream.Position;
        WriteData(bw);
        WriteSize(bw);
    }

    protected virtual void WriteData(BinaryWriter bw)
    {
        
    }

    private void WriteSize(BinaryWriter bw)
    {
        var off = bw.BaseStream.Position;
        bw.Seek((int) DataOffset - 4, SeekOrigin.Begin);
        bw.Write(Byteswap((uint) (off - DataOffset - 8)));
        bw.Seek((int) off + 4, SeekOrigin.Begin);
    }

    private uint Byteswap(uint value)
    {
        value = value >> 16 | value << 16;
        return (value & 0xFF00_FF00) >> 8 | (uint)(((int)value & 0x00FF_00FF) << 8);
    }
}
