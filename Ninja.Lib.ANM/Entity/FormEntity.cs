﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.ANM.Entity;

public class FormEntity<T> : Entity where T : Entity, new()
{
    public T Data { get; set; }

    public FormEntity()
    {
        ExpectedTag = "FORM";
    }

    protected override void ReadData(BinaryReader br)
    {
        Data = new T();
        Data.Read(br);
    }

    protected override void WriteData(BinaryWriter bw)
    {
        Data.Write(bw);
    }
}
