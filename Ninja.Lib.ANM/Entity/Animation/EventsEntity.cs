﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.ANM.Entity.Animation;

public class EventsEntity : Entity
{
    public List<AnimEvent> AnimEvents { get; set; } = new();

    public EventsEntity()
    {
        ExpectedTag = "EVNT";
    }

    protected override void ReadData(BinaryReader br)
    {
        var count = br.ReadUInt16();
        for (var index = 0; index < count; ++index)
        {
            AnimEvents.Add(new AnimEvent
            {
                Frame = br.ReadInt32(),
                Name = br.ReadRawString(br.ReadInt32()),
                UnkValue1 = br.ReadRawString(br.ReadInt32()),
                UnkValue2 = br.ReadInt32()
            });
        }
    }

    protected override void WriteData(BinaryWriter bw)
    {
        bw.Write((ushort) AnimEvents.Count);
        foreach (var animEvent in AnimEvents)
        {
            bw.Write(animEvent.Frame);
            bw.Write(animEvent.Name.Length + 1);
            bw.WriteCString(animEvent.Name);
            bw.Write(animEvent.UnkValue1.Length + 1);
            bw.WriteCString(animEvent.UnkValue1);
            bw.Write(animEvent.UnkValue2);
        }
    }
}
