﻿namespace Ninja.Lib.ANM.Entity.Animation;

public class DataEntity : Entity
{
    private const double RadPerVal = 1.0 / ushort.MaxValue;
    public Dictionary<string, BoneAnimation> BoneAnimations { get; set; } = new();

    private HeadEntity _headEntity;

    public DataEntity(HeadEntity headEntity)
    {
        ExpectedTag = "DATA";
        _headEntity = headEntity;
    }

    protected override void ReadData(BinaryReader br)
    {
        foreach (var bone in _headEntity.Bones)
        {
            var boneAnim = new BoneAnimation
            {
                FrameCount = bone.NumFrames
            };
            ReadTranslations(br, bone, boneAnim);
            ReadScales(br, bone, boneAnim);
            ReadRotations(br, bone, boneAnim);
            BoneAnimations.Add(bone.Name, boneAnim);
        }
    }

    private void ReadTranslations(BinaryReader br, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneHead.TranslationFrameCount <= 0)
            return;

        boneAnimation.Translations = new Dictionary<ushort, Vector3>();

        var translationIds = new List<ushort>();
        for (var index = 0; index < boneHead.TranslationFrameCount; ++index)
        {
            translationIds.Add(br.ReadUInt16());
        }

        var multiplier = boneHead.TranslationMultiplier * RadPerVal;

        for (var index = 0; index < boneHead.TranslationFrameCount; ++index)
        {
            var x = (float)(br.ReadUInt16() * multiplier + boneHead.TranslationBias);
            var y = (float)(br.ReadUInt16() * multiplier + boneHead.TranslationBias);
            var z = (float)(br.ReadUInt16() * multiplier + boneHead.TranslationBias);

            boneAnimation.Translations.Add(translationIds[index], new Vector3(x, y, z));
        }
    }

    private void ReadRotations(BinaryReader br, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneHead.RotationFrameCount <= 0)
            return;

        boneAnimation.Rotations = new Dictionary<ushort, Quaternion>();

        var rotationIds = new List<ushort>();
        for (var index = 0; index < boneHead.RotationFrameCount; ++index)
        {
            rotationIds.Add(br.ReadUInt16());
        }

        var multiplier = boneHead.RotationMultiplier * RadPerVal;

        for (var index = 0; index < boneHead.RotationFrameCount; ++index)
        {
            var x = br.ReadUInt16() * multiplier + boneHead.RotationBias;
            var y = br.ReadUInt16() * multiplier + boneHead.RotationBias;
            var z = br.ReadUInt16() * multiplier + boneHead.RotationBias;
            var w = br.ReadUInt16() * multiplier + boneHead.RotationBias;

            boneAnimation.Rotations.Add(rotationIds[index], new Quaternion(x, y, z, w));
        }
    }

    private void ReadScales(BinaryReader br, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneHead.ScaleFrameCount <= 0)
            return;

        boneAnimation.Scales = new Dictionary<ushort, Vector3>();

        var scaleIds = new List<ushort>();
        for (var index = 0; index < boneHead.ScaleFrameCount; ++index)
        {
            scaleIds.Add(br.ReadUInt16());
        }

        var multiplier = boneHead.ScaleMultiplier * RadPerVal;

        for (var index = 0; index < boneHead.ScaleFrameCount; ++index)
        {
            var x = (float)(br.ReadUInt16() * multiplier + boneHead.ScaleBias);
            var y = (float)(br.ReadUInt16() * multiplier + boneHead.ScaleBias);
            var z = (float)(br.ReadUInt16() * multiplier + boneHead.ScaleBias);

            boneAnimation.Scales.Add(scaleIds[index], new Vector3(x, y, z));
        }
    }

    protected override void WriteData(BinaryWriter bw)
    {
        foreach (var bone in _headEntity.Bones)
        {
            var boneAnim = BoneAnimations[bone.Name];
            WriteTranslations(bw, bone, boneAnim);
            WriteScales(bw, bone, boneAnim);
            WriteRotations(bw, bone, boneAnim);
        }
    }

    private void WriteTranslations(BinaryWriter bw, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneAnimation.Translations == null || boneAnimation.Translations.Count == 0)
            return;

        foreach (var (key, _) in boneAnimation.Translations)
        {
            bw.Write(key);
        }

        var multiplier = 1.0f;
        if (boneHead.ScaleMultiplier > 0.0)
        {
            multiplier = ushort.MaxValue / boneHead.TranslationMultiplier;
        }

        foreach (var (_, pos) in boneAnimation.Translations)
        {
            bw.Write(Convert(pos.X, boneHead.TranslationBias, multiplier));
            bw.Write(Convert(pos.Y, boneHead.TranslationBias, multiplier));
            bw.Write(Convert(pos.Z, boneHead.TranslationBias, multiplier));
        }
    }

    private void WriteRotations(BinaryWriter bw, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneAnimation.Rotations == null || boneAnimation.Rotations.Count == 0)
            return;

        foreach (var (key, _) in boneAnimation.Rotations)
        {
            bw.Write(key);
        }
        
        var multiplier = 1.0f;
        if (boneHead.ScaleMultiplier > 0.0)
        {
            multiplier = ushort.MaxValue / boneHead.RotationMultiplier;
        }

        foreach (var (_, pos) in boneAnimation.Rotations)
        {
            bw.Write(Convert(pos.X, boneHead.RotationBias, multiplier));
            bw.Write(Convert(pos.Y, boneHead.RotationBias, multiplier));
            bw.Write(Convert(pos.Z, boneHead.RotationBias, multiplier));
            bw.Write(Convert(pos.W, boneHead.RotationBias, multiplier));
        }
    }

    private void WriteScales(BinaryWriter bw, BoneHead boneHead, BoneAnimation boneAnimation)
    {
        if (boneAnimation.Scales == null || boneAnimation.Scales.Count == 0)
            return;

        foreach (var (key, _) in boneAnimation.Scales)
        {
            bw.Write(key);
        }
        
        var multiplier = 1.0f;
        if (boneHead.ScaleMultiplier > 0.0)
        {
            multiplier = ushort.MaxValue / boneHead.ScaleMultiplier;
        }

        foreach (var (_, pos) in boneAnimation.Scales)
        {
            bw.Write(Convert(pos.X, boneHead.ScaleBias, multiplier));
            bw.Write(Convert(pos.Y, boneHead.ScaleBias, multiplier));
            bw.Write(Convert(pos.Z, boneHead.ScaleBias, multiplier));
        }
    }

    private ushort Convert(double axis, float bias, double multiplier)
    {
        return (ushort)Math.Min(Math.Max((axis - bias) * multiplier, 0.0f), ushort.MaxValue);
    }
}
