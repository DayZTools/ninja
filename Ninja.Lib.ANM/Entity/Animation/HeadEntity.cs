﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.ANM.Entity.Animation;

public class HeadEntity : Entity
{
    public List<BoneHead> Bones { get; set; } = new();

    private readonly string _type;

    public HeadEntity(string type)
    {
        _type = type;
        ExpectedTag = "HEAD";
    }

    protected override void ReadData(BinaryReader br)
    {
        switch (_type)
        {
            case "ANIMSET5":
                Read5(br);
                break;
            case "ANIMSET6":
                Read6(br);
                break;
            default:
                throw new InvalidOperationException("Unknown model type: " + _type);
        }
    }

    private void Read5(BinaryReader br)
    {
        while (br.BaseStream.Position < EntrySize + DataOffset)
        {
            Bones.Add(new BoneHead
            {
                Name = br.ReadRawString(32),
                TranslationBias = br.ReadSingle(),
                TranslationMultiplier = br.ReadSingle(),
                RotationBias = br.ReadSingle(),
                RotationMultiplier = br.ReadSingle(),
                NumFrames = br.ReadUInt16(),
                TranslationFrameCount = br.ReadUInt16(),
                RotationFrameCount = br.ReadUInt16(),
                Flags = br.ReadInt16()
            });
        }
    }

    private void Read6(BinaryReader br)
    {
        while (br.BaseStream.Position < EntrySize + DataOffset)
        {
            Bones.Add(new BoneHead
            {
                TranslationBias = br.ReadSingle(),
                TranslationMultiplier = br.ReadSingle(),
                RotationBias = br.ReadSingle(),
                RotationMultiplier = br.ReadSingle(),
                ScaleBias = br.ReadSingle(),
                ScaleMultiplier = br.ReadSingle(),
                NumFrames = br.ReadUInt16(),
                TranslationFrameCount = br.ReadUInt16(),
                RotationFrameCount = br.ReadUInt16(),
                ScaleFrameCount = br.ReadUInt16(),
                Flags = br.ReadByte(),
                Name = br.ReadRawString(br.ReadByte())
            });
        }
    }

    protected override void WriteData(BinaryWriter bw)
    {
        switch (_type)
        {
            case "ANIMSET5":
                Write5(bw);
                break;
            case "ANIMSET6":
                Write6(bw);
                break;
            default:
                throw new InvalidOperationException("Unknown model type: " + _type);
        }
    }

    private void Write6(BinaryWriter bw)
    {
        foreach (var bone in Bones)
        {
            bw.Write(bone.TranslationBias);
            bw.Write(bone.TranslationMultiplier);
            bw.Write(bone.RotationBias);
            bw.Write(bone.RotationMultiplier);
            bw.Write(bone.ScaleBias);
            bw.Write(bone.ScaleMultiplier);
            bw.Write(bone.NumFrames);
            bw.Write(bone.TranslationFrameCount);
            bw.Write(bone.RotationFrameCount);
            bw.Write(bone.ScaleFrameCount);
            bw.Write(bone.Flags);
            bw.Write((byte) bone.Name.Length);
            bw.WriteRawString(bone.Name, bone.Name.Length);
        }
    }
    
    private void Write5(BinaryWriter bw)
    {
        foreach (var bone in Bones)
        {
            bw.WriteRawString(bone.Name, 32);
            bw.Write(bone.TranslationBias);
            bw.Write(bone.TranslationMultiplier);
            bw.Write(bone.RotationBias);
            bw.Write(bone.RotationMultiplier);
            bw.Write(bone.NumFrames);
            bw.Write(bone.TranslationFrameCount);
            bw.Write(bone.RotationFrameCount);
            bw.Write((ushort) bone.Flags);
        }
    }
}
