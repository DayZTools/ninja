﻿namespace Ninja.Lib.ANM.Entity.Animation;

public class AnimationEntity : Entity
{
    public FpsEntity? Fps { get; set; }
    public HeadEntity? Head { get; set; }

    public DataEntity? Data { get; set; }

    public EventsEntity? Events { get; set; }

    public AnimationEntity()
    {
        TagLen = 8;
        ExpectedTag = "ANIMSET6";
    }

    public AnimationEntity(bool oldType) : this()
    {
        ExpectedTag = oldType ? "ANIMSET5" : "ANIMSET6";
    }

    protected override void ValidateTag()
    {
        // to override error
    }

    protected override void ReadData(BinaryReader br)
    {
        Fps = new FpsEntity();
        Fps.Read(br);

        Head = new HeadEntity(Tag);
        Head.Read(br);

        Data = new DataEntity(Head);
        Data.Read(br);

        Events = new EventsEntity();
        Events.Read(br);
    }

    protected override void WriteData(BinaryWriter bw)
    {
        Fps?.Write(bw);
        Head?.Write(bw);
        Data?.Write(bw);
        Events?.Write(bw);
    }

    public void Create(Dictionary<string, BoneAnimation> data, List<AnimEvent> events, int fps)
    {
        Fps = new FpsEntity
        {
            FPS = fps
        };
        Head = new HeadEntity(ExpectedTag);
        foreach (var (boneName, anim) in data)
        {
            var bone = new BoneHead
            {
                Name = boneName,
                TranslationFrameCount = (ushort) (anim.Translations?.Count ?? 0),
                RotationFrameCount = (ushort) (anim.Rotations?.Count ?? 0),
                ScaleFrameCount = (ushort) (anim.Scales?.Count ?? 0),
                NumFrames = anim.FrameCount,
                Flags = 0 // TODO: wtf is this?
            };
            
            if (anim.Translations != null)
            {
                var (min, max) = FindMinMax(anim.Translations);
                bone.TranslationBias = (float)min;
                bone.TranslationMultiplier = GetMultiplier(min, max);
            }

            if (anim.Rotations != null)
            {
                var (min, max) = FindMinMax(anim.Rotations);
                bone.RotationBias = (float)min;
                bone.RotationMultiplier = GetMultiplier(min, max);
            }
            
            if (anim.Scales != null)
            {
                var (min, max) = FindMinMax(anim.Scales);
                bone.ScaleMultiplier = (float)min;
                bone.ScaleBias = GetMultiplier(min, max);
            }
            
            Head.Bones.Add(bone);
        }
        
        Data = new DataEntity(Head)
        {
            BoneAnimations = data
        };

        if (events.Count > 0)
        {
            Events = new EventsEntity
            {
                AnimEvents = events
            };
        }
    }

    private float GetMultiplier(double min, double max)
    {
        const float epsilon = 9.9999999747524271E-07f;
        return (float) (Math.Abs(max - min) < epsilon ? 1.0 : max - min);
    }

    private (double, double) FindMinMax<T>(Dictionary<ushort, T> collection)
    {
        if (collection is Dictionary<ushort, Quaternion> quaternions)
        {
            var minValue = quaternions.Values.SelectMany(q => new[] { q.X, q.Y, q.Z, q.W }).Min();
            var maxValue = quaternions.Values.SelectMany(q => new[] { q.X, q.Y, q.Z, q.W }).Max();
            return (minValue, maxValue);
        }

        if (collection is Dictionary<ushort, Vector3> vectors)
        {
            var minValue = vectors.Values.SelectMany(v => new[] { v.X, v.Y, v.Z }).Min();
            var maxValue = vectors.Values.SelectMany(v => new[] { v.X, v.Y, v.Z }).Max();
            return (minValue, maxValue);
        }

        throw new ArgumentException("Invalid generic type", nameof(collection));
    }
}
