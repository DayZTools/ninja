﻿namespace Ninja.Lib.ANM.Entity.Animation;

public class FpsEntity : Entity
{
    public int FPS { get; set; }

    public FpsEntity()
    {
        ExpectedTag = "FPS";
    }

    protected override void ReadData(BinaryReader br)
    {
        FPS = br.ReadInt32();
    }

    protected override void WriteData(BinaryWriter bw)
    {
        bw.Write(FPS);
    }
}
