﻿namespace Ninja.Lib.ANM;

public class Translation
{
    public int FrameId { get; set; }
    public Vector3 Value { get; set; }
}
