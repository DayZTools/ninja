﻿namespace Ninja.Lib.ANM;

public class Scale
{
    public int FrameId { get; set; }
    public Vector3 Value { get; set; }
}
