﻿namespace Ninja.Lib.ANM;

public class Rotation
{
    public int FrameId { get; set; }
    public Quaternion Value { get; set; }
}
