﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommandLine;
using PboNinja.Commands;

namespace PboNinja;

class PboNinja
{
    /// <summary>
    /// Entry point of PboViewer
    /// </summary>
    static int Main(string[] args)
    {
        var ti = typeof(ICommand);
        var commands = new List<Type>();
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            commands.AddRange(assembly.GetTypes().Where(t => ti.IsAssignableFrom(t) && !t.IsInterface));
        }

        var parser = Parser.Default.ParseArguments(args, commands.ToArray());

        var x = parser.Value as ICommand;

        return x?.Run() ?? ExitCode.Success;
    }
}
