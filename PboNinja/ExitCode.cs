namespace PboNinja;

public static class ExitCode
{
    /// <summary>
    /// Success code
    /// </summary>
    public const int Success = 0;

    /// <summary>
    /// Failure code
    /// </summary>
    public const int Failure = 2;
}