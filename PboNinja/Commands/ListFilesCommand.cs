﻿using System;
using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;
using ConsoleTables;
using Ninja.Lib.Pbo;

namespace PboNinja.Commands;

[Verb("list", HelpText = "List files of a PBO.")]
class ListFilesCommand: ICommand
{
    /// <summary>
    /// Directory to pack
    /// </summary>
    [Value(0, HelpText = "The full path of the PBO.")]
    public string Path { get; set; }

    [Usage]
    public static IEnumerable<Example> Examples =>
        new List<Example>()
        {
            new("List files of a PBO", new ListFilesCommand { Path = "file.pbo" })
        };



    public int Run()
    {
        try
        {
            // Pack the folder as a PBO
            using var pboSharpClient = new PboClient();
            var currentPbo = pboSharpClient.AnalyzePBO(Path);

            if (currentPbo == null)
            {
                Console.WriteLine("Failed to read PBO.");
                return ExitCode.Failure;
            }

            Console.WriteLine("PboSummary:");
            Console.WriteLine($"Path: {currentPbo.FilePath}");
            var porpTalbe = new ConsoleTable("Key", "Value");
           
            foreach (var prop in currentPbo.Properties)
            {
                porpTalbe.AddRow(prop.Key, prop.Value);
            }
            
            Console.WriteLine(porpTalbe.ToMinimalString());
            var table = new ConsoleTable("Path", "Original size", "Data size", "Change date", "Is compressed");

            foreach (var x in currentPbo.Files)
            {
                var packed = x.MimeType == MimeType.Compressed ? 'Y' : 'N';
                var time = new DateTime(1970, 1, 1);
                time = time.AddSeconds(x.Timestamp).ToLocalTime();
                table.AddRow(x.FileName, x.OriginalSize, x.DataSize, time, packed);
            }
            
            Console.WriteLine(table.ToMinimalString());

            return ExitCode.Success;
        }
        catch (Exception ex) {
            Console.WriteLine($"Unexpected error: {Environment.NewLine}{ex}");
            return ExitCode.Failure;
        }
    }
}
