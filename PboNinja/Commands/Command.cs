﻿namespace PboNinja.Commands;

public interface ICommand
{
    int Run();
}
