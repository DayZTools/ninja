﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using CommandLine.Text;
using Ninja.Lib.Pbo;
using Ninja.Lib.Utils;

namespace PboNinja.Commands;

/// <summary>
/// Pack a folder as a PBO
/// </summary>
[Verb("pack", HelpText = "Pack a PBO.")]
class PackFolderCommand : ICommand
{
    /// <summary>
    /// Directory to pack
    /// </summary>

    [Value(0, HelpText = "The full path to the source folder.")]
    public string Path { get; set; }
    
    [Option('o', "output", HelpText = "Output path where to save PBO file.")]
    public string OutputPath { get; set; }

    [Option('c', "compress", HelpText = "List of files to compress. Patterns are supported.", Separator = ',')]
    public IEnumerable<string>? CompressList { get; set; }

    [Option('e', "exclude", HelpText = "List of files to exclude from process. Patterns are supported.")]
    public IEnumerable<string>? ExclusionList { get; set; }
    
    [Option('b', "binarize", HelpText = "Binarize P3D, cpp files and convert textures to PAA")]
    public bool Binarize { get; set; }

    [Usage]
    public static IEnumerable<Example> Examples =>
        new List<Example>()
        {
            new("Pack a PBO", new PackFolderCommand { Path = "P:\\MyMod" }),
            new("Pack a PBO with compression, exclusion list and the output",
                new PackFolderCommand
                {
                    Path = "P:\\MyMod",
                    CompressList = new List<string> { "*.c", "*.paa", "*.p3d" },
                    ExclusionList = new List<string> {"Readme.txt", "model.cfg", "Todo list.txt"},
                    OutputPath = "P:\\Mods\\MyMod\\addons"
                }),
            
        };

    public int Run()
    {
        try
        {
            // If the file exists, delete it
            var path = System.IO.Path.Join(System.IO.Path.GetDirectoryName(Path), System.IO.Path.GetFileName(Path) + ".pbo");
            if (File.Exists(path))
                File.Delete(System.IO.Path.Join(path));

            // Pack the folder as a PBO
            using var pboSharpClient = new PboClient();
            pboSharpClient.OnEvent += args =>
            {
                if (args.Type == EventType.Error)
                    Console.WriteLine(args.Message);
            };

            if (OutputPath == null)
            {
                OutputPath = System.IO.Path.GetDirectoryName(Path);
            }
            var ok = pboSharpClient.PackPBO(Path, OutputPath,
                System.IO.Path.GetFileName(Path),
                CompressList?.ToArray()
                );

            if (ok)
                Console.WriteLine($"Pbo successful pack at the path: {OutputPath}");

            return ExitCode.Success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected error: {Environment.NewLine}{ex}");
            return ExitCode.Failure;
        }
    }
}
