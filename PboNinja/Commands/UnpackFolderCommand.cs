﻿using System;
using System.Collections.Generic;
using System.IO;
using CommandLine;
using CommandLine.Text;
using Ninja.Lib.Pbo;

namespace PboNinja.Commands;

/// <summary>
/// Unpack PBO
/// </summary>
[Verb("unpack", HelpText = "Unpack a PBO.")]
class UnpackFolderCommand : ICommand
{
    /// <summary>
    /// Directory to pack
    /// </summary>
    [Value(0, HelpText = "The full path of the PBO.")]
    public string Path { get; set; }

    [Usage]
    public static IEnumerable<Example> Examples =>
        new List<Example>()
        {
            new("Unpack a PBO", new UnpackFolderCommand { Path = "file.pbo" })
        };

    public int Run()
    {
        try
        {
            // If the destination directory exists, delete it
            var path = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), System.IO.Path.GetFileNameWithoutExtension(Path));
            if (Directory.Exists(path))
                Directory.Delete(path, true);

            // Pack the folder as a PBO
            using var pboSharpClient = new PboClient();
            pboSharpClient.ExtractAll(Path, path);

            Console.WriteLine($"Pbo successful unpack at the path: {path}");

            return ExitCode.Success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected error: {Environment.NewLine}{ex}");
            return ExitCode.Failure;
        }
    }
}
