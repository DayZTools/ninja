﻿namespace Ninja.Lib.PAA;

public enum PaaType
{
    DXT1 = 0xff01,
    DXT2 = 0xff02,
    DXT3 = 0xff03,
    DXT4 = 0xff04,
    DXT5 = 0xff05,
    RGBA_5551 = 0x1555,
    RGBA_4444 = 0x4444,
    RGBA_8888 = 0x8888,
    AI88 = 0x8080
}
