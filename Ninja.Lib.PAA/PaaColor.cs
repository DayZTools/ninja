﻿namespace Ninja.Lib.PAA;

public class PaaColor
{
    private uint value;

    public byte Alpha => (byte)((value >> 24) & 0xff);
    public byte Red => (byte)((value >> 16) & 0xff);
    public byte Green => (byte)((value >> 8) & 0xff);
    public byte Blue => (byte)((value) & 0xff);

    public PaaColor(uint value)
    {
        this.value = value;
    }

    public PaaColor(byte red, byte green, byte blue, byte alpha = 0xff)
    {
        value = ColorToUint(red, green, blue, alpha);
    }

    public PaaColor(float red, float green, float blue, float alpha)
    {
        value = ColorToUint((byte)(red * 255),
            (byte)(green * 255),
            (byte)(blue * 255),
            (byte)(alpha * 255));
    }

    public uint Color => value;

    private static uint ColorToUint(byte r, byte g, byte b, byte a)
    {
        return (uint)(a << 24 | r << 16 | g << 8) | b;
    }
}
