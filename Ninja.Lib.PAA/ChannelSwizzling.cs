﻿namespace Ninja.Lib.PAA;

internal enum TexSwizzle : byte
{
    TSAlpha,
    TSRed,
    TSGreen,
    TSBlue,
    TSInvAlpha,
    TSInvRed,
    TSInvGreen,
    TSInvBlue,
    TSOne
}

internal struct ARGBSwizzle
{
    internal TexSwizzle SwizB;
    internal TexSwizzle SwizG;
    internal TexSwizzle SwizR;
    internal TexSwizzle SwizA;

    internal TexSwizzle this[int ch]
    {
        get
        {
            switch (ch)
            {
                case 0: return SwizA;
                case 1: return SwizR;
                case 2: return SwizG;
                case 3: return SwizB;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        set
        {
            switch (ch)
            {
                case 0:
                    SwizA = value;
                    break;
                case 1:
                    SwizR = value;
                    break;
                case 2:
                    SwizG = value;
                    break;
                case 3:
                    SwizB = value;
                    break;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }


    static ARGBSwizzle()
    {
        Default.SwizA = TexSwizzle.TSAlpha;
        Default.SwizR = TexSwizzle.TSRed;
        Default.SwizG = TexSwizzle.TSGreen;
        Default.SwizB = TexSwizzle.TSBlue;
    }

    internal static ARGBSwizzle Default;
}

internal static class ChannelSwizzling
{
    internal static void Apply(byte[] pixelData, ARGBSwizzle swizzle)
    {
        var invSwizzle = ARGBSwizzle.Default;
        InvertSwizzle(ref invSwizzle, in swizzle, 0);
        InvertSwizzle(ref invSwizzle, in swizzle, 1);
        InvertSwizzle(ref invSwizzle, in swizzle, 2);
        InvertSwizzle(ref invSwizzle, in swizzle, 3);
        ChannelSwizzle(in invSwizzle, pixelData);
    }

    private static void InvertSwizzle(ref ARGBSwizzle invSwizzle, in ARGBSwizzle swizzle, byte ch)
    {
        var swiz = TexSwizzle.TSAlpha + ch;
        switch (swizzle[ch])
        {
            case >= TexSwizzle.TSInvAlpha and <= TexSwizzle.TSInvBlue:
                invSwizzle[swizzle[ch] - TexSwizzle.TSInvAlpha] = TexSwizzle.TSInvAlpha - TexSwizzle.TSAlpha + swiz;
                break;
            case <= TexSwizzle.TSBlue:
                invSwizzle[(int)swizzle[ch]] = swiz;
                break;
        }
    }

    private static (int offset, int mul, int add) CheckInvSwizzle(TexSwizzle swiz)
    {
        if (swiz == TexSwizzle.TSOne)
        {
            // one - ignore input (mul by 0) and set it to one (add 255)
            return (0, 0, 255);
        }

        var mul = 1;
        var add = 0;
        switch (swiz)
        {
            case TexSwizzle.TSInvAlpha:
                swiz = TexSwizzle.TSAlpha;
                mul = -1;
                add = 255;
                break;
            case TexSwizzle.TSInvRed:
                swiz = TexSwizzle.TSRed;
                mul = -1;
                add = 255;
                break;
            case TexSwizzle.TSInvGreen:
                swiz = TexSwizzle.TSGreen;
                mul = -1;
                add = 255;
                break;
            case TexSwizzle.TSInvBlue:
                swiz = TexSwizzle.TSBlue;
                mul = -1;
                add = 255;
                break;
        }

        var offset = swiz < TexSwizzle.TSOne ? 24 - (int)swiz * 8 : 0;

        return (offset, mul, add);
    }

    private static void ChannelSwizzle(in ARGBSwizzle channelSwizzle, byte[] argbPixels)
    {
        if (channelSwizzle[0] == TexSwizzle.TSAlpha && channelSwizzle[1] == TexSwizzle.TSRed &&
            channelSwizzle[2] == TexSwizzle.TSGreen && channelSwizzle[3] == TexSwizzle.TSBlue)
        {
            return;
        }

        var (aOffset, mulA, addA) = CheckInvSwizzle(channelSwizzle[0]);
        var (rOffset, mulR, addR) = CheckInvSwizzle(channelSwizzle[1]);
        var (gOffset, mulG, addG) = CheckInvSwizzle(channelSwizzle[2]);
        var (bOffset, mulB, addB) = CheckInvSwizzle(channelSwizzle[3]);

        var nPixel = argbPixels.Length / 4;
        while (--nPixel >= 0)
        {
            var pixOffset = nPixel * 4;
            var p = BitConverter.ToInt32(argbPixels, pixOffset);
            var a = (p >> aOffset) & 0xff;
            var r = (p >> rOffset) & 0xff;
            var g = (p >> gOffset) & 0xff;
            var b = (p >> bOffset) & 0xff;

            argbPixels[pixOffset] = (byte)(b * mulB + addB);
            argbPixels[pixOffset + 1] = (byte)(g * mulG + addG);
            argbPixels[pixOffset + 2] = (byte)(r * mulR + addR);
            argbPixels[pixOffset + 3] = (byte)(a * mulA + addA);
        }
    }
}
