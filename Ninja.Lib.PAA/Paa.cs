﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.PAA;

public class Paa
{
    public PaaType Type { get; private set; }

    public bool IsAlpha { get; private set; }
    public bool IsTransparent { get; private set; }
    public PaaColor? AverageColor { get; private set; }
    public PaaColor? MaxColor { get; private set; }
    public Palette Palette { get; private set; } = new();

    public List<Mipmap> Mipmaps { get; private set; }
    
    private ARGBSwizzle ChannelSwizzle { get; set; } = ARGBSwizzle.Default;

    public void Read(Stream stream)
    {
        var br = new BinaryReader(stream);
        Type = (PaaType)br.ReadUInt16();

        uint[]? mipMapOffsets = null;
        while (br.ReadRawString(4) == "GGAT")
        {
            var name = new string(br.ReadRawString(4).Reverse().ToArray());
            var len = br.ReadInt32();

            switch (name)
            {
                case "AVGC":
                    AverageColor = new PaaColor(br.ReadUInt32());
                    break;
                case "MAXC":
                    MaxColor = new PaaColor(br.ReadUInt32());
                    break;
                case "FLAG":
                    var flag = br.ReadInt32();
                    if ((flag & 0x1) != 0) IsAlpha = true;
                    if ((flag & 0x2) != 0) IsTransparent = true;
                    break;
                case "SWIZ":
                    ChannelSwizzle = new ARGBSwizzle
                    {
                        SwizA = (TexSwizzle)br.ReadByte(),
                        SwizR = (TexSwizzle)br.ReadByte(),
                        SwizG = (TexSwizzle)br.ReadByte(),
                        SwizB = (TexSwizzle)br.ReadByte()
                    };
                    break;
                case "PROC":
                    br.BaseStream.Seek(len, SeekOrigin.Current); // skip text
                    break;
                case "OFFS":
                    var nOffsets = len / 4;
                    mipMapOffsets = new uint[nOffsets];
                    for (var i = 0; i < nOffsets; ++i)
                        mipMapOffsets[i] = br.ReadUInt32();
                    break;
                default:
                    throw new Exception("Got unknown tag: " + name);
            }
        }

        br.BaseStream.Seek(-4, SeekOrigin.Current);
        Palette.Read(br);

        if (mipMapOffsets != null)
        {
            Mipmaps = new List<Mipmap>();
            for (var i = 0; br.ReadInt32() != 0; ++i)
            {
                br.BaseStream.Seek(-4, SeekOrigin.Current);
                var mipmap = new Mipmap();
                mipmap.Read(br, mipMapOffsets[i]);
                Mipmaps.Add(mipmap);
            }
        }

        var x = br.ReadUInt16();
        if (x != 0)
            throw new Exception("Invalid format: terminator bytes not found");
    }

    public void Write(Stream output)
    {
        var bw = new BinaryWriter(output);
        bw.Write((ushort) Type);
        
        if (AverageColor != null)
        {
            WriteTagHdr(bw, "AVGC", 4);
            bw.Write(AverageColor.Color);
        }

        if (MaxColor != null)
        {
            WriteTagHdr(bw, "MAXC", 4);
            bw.Write(MaxColor.Color);
        }

        WriteTagHdr(bw, "FLAG", 4);
        uint flg = 0;
        if (IsAlpha)
        {
            flg |= 0x1;
        }

        if (IsTransparent)
        {
            flg |= 0x2;
        }

        bw.Write(flg);
        
        WriteTagHdr(bw, "SWIZ", 4);
        bw.Write((byte) ChannelSwizzle.SwizA);
        bw.Write((byte) ChannelSwizzle.SwizR);
        bw.Write((byte) ChannelSwizzle.SwizG);
        bw.Write((byte) ChannelSwizzle.SwizB);
        
        var offsSize = Mipmaps.Count * 4;
        WriteTagHdr(bw, "OFFS", offsSize);
        var endOfOffs = bw.BaseStream.Position + offsSize + 8 + 4;
        var curMipOffset = endOfOffs;
        foreach (var mipmap in Mipmaps)
        {
            
        }
        // todo write offsets
        
        Palette.Write(bw);
        
        // todo write mipmaps
        throw new NotImplementedException();
    }

    private void WriteTagHdr(BinaryWriter bw, string tag, int len)
    {
        bw.WriteRawString("GGAT");
        var rtag = tag.Reverse().ToString();
        bw.WriteRawString(rtag);
        bw.Write(len);
    }

    public byte[] GetArgb32PixelData(Stream stream, int mipLevel = 0)
    {
        if (mipLevel < 0 || mipLevel > Mipmaps.Count)
        {
            throw new ArgumentOutOfRangeException(nameof(mipLevel), mipLevel, "out of range");
        }
        var data = Mipmaps[mipLevel].GetRgba32PixelData(stream, Type);
        ChannelSwizzling.Apply(data, ChannelSwizzle);
        return data;
    }
}
