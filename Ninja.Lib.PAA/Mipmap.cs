using Ninja.Lib.Compression.Lzss;
using Ninja.Lib.Compression.MiniLzo;
using Ninja.Lib.Utils;

namespace Ninja.Lib.PAA;

public class Mipmap
{
    public ushort Width { get; private set; }
    public ushort Height { get; private set; }

    public bool IsLzss { get; private set; }
    public bool IsLzo { get; private set; }

    private long _dataOffset;
    private int _dataSize;

    public void Read(BinaryReader br, uint offset)
    {
        Width = br.ReadUInt16();
        Height = br.ReadUInt16();

        if (Width == 1234 && Height == 8765) // special 1234 x 8765 signature
        {
            Width = br.ReadUInt16();
            Height = br.ReadUInt16();
            IsLzss = true;
        }

        // Arma 2 introduces LZO compression for DXT.
        // The criteria for DXT-LZO compression is having the top bit of the width paramater set.
        if ((Width & 0x8000) != 0)
        {
            Width &= 0x7FFF;
            IsLzo = true;
        }

        _dataSize = br.ReadInt24();
        _dataOffset = br.BaseStream.Position;
        br.BaseStream.Seek(_dataSize, SeekOrigin.Current);
    }

    public void Write(BinaryWriter bw)
    {
        if (IsLzss)
        {
            bw.Write(1234);
            bw.Write(8765);
        }
        if (IsLzo)
        {
            bw.Write(Width | 0x8000);
        }
        else
        {
            bw.Write(Width);
        }
        bw.Write(Height);
        
        
        
        throw new NotImplementedException();
    }

    public byte[] GetRawPixelData(Stream stream, PaaType type)
    {
        stream.Seek(_dataOffset, SeekOrigin.Begin);

        var uncompressedSize = Width * Height;
        var br = new BinaryReader(stream);

        switch (type)
        {
            case PaaType.DXT1:
                uncompressedSize /= 2;
                goto case PaaType.DXT2;
            case PaaType.DXT2:
            case PaaType.DXT3:
            case PaaType.DXT4:
            case PaaType.DXT5:
                if (!IsLzo) uncompressedSize = _dataSize;
                break;
            case PaaType.RGBA_5551:
            case PaaType.RGBA_4444:
            case PaaType.AI88:
                uncompressedSize *= 2;
                IsLzss = uncompressedSize > 1023;
                break;
            case PaaType.RGBA_8888:
                uncompressedSize *= 4;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }

        byte[] data;

        if (IsLzo)
        {
            var @in = br.ReadBytes(_dataSize);
            data = new byte[uncompressedSize];
            MiniLZO.Decompress(@in, ref data);
        }
        else if (IsLzss)
        {
            Decompressor.Decompress(stream, out data, uncompressedSize, true);
        }
        else
        {
            data = br.ReadBytes(_dataSize);
        }

        return data;
    }

    public byte[] GetRgba32PixelData(Stream stream, PaaType type)
    {
        var data = GetRawPixelData(stream, type);
        return PixelFormatConversion.ConvertToARGB32(data, Width, Height, type);
    }
}
