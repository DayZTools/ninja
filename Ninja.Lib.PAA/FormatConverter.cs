﻿using System.Runtime.CompilerServices;

namespace Ninja.Lib.PAA;

public static class PixelFormatConversion
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static void SetColor(IList<byte> img, int offset, byte a, byte r, byte g, byte b)
    {
        img[offset] = b;
        img[offset + 1] = g;
        img[offset + 2] = r;
        img[offset + 3] = a;
    }

    public static byte[] Argb16ToArgb32(byte[] src)
    {
        var dst = new byte[src.Length * 2];
        var nPixel = src.Length / 2;
        for (var index = 0; index < nPixel; ++index)
        {
            var hbyte = src[index * 2 + 1];
            var lbyte = src[index * 2];
            var lhbyte = (byte)(hbyte & 0x0F);
            var hhbyte = (byte)((hbyte & 0xF0) >> 4);
            var llbyte = (byte)(lbyte & 0x0F);
            var hlbyte = (byte)((lbyte & 0xF0) >> 4);
            var b = (byte)(lhbyte * byte.MaxValue / 15);
            var a = (byte)(hhbyte * byte.MaxValue / 15);
            var r = (byte)(llbyte * byte.MaxValue / 15);
            var g = (byte)(hlbyte * byte.MaxValue / 15);

            SetColor(dst, index * 4, a, r, g, b);
        }

        return dst;
    }

    public static byte[] Argb1555ToArgb32(byte[] src)
    {
        var dst = new byte[src.Length * 2];
        var nPixel = src.Length / 2;
        for (var index = 0; index < nPixel; ++index)
        {
            var s = BitConverter.ToUInt16(src, index * 2);
            var abit = ((s & 0x8000) >> 15) == 1;
            var b5bit = (byte)(s & 0x001F);
            var g5bit = (byte)((s & 0x03E0) >> 5);
            var r5bit = (byte)((s & 0x7C00) >> 10);
            var b = (byte)(r5bit * byte.MaxValue / 31);
            var a = (abit) ? byte.MaxValue : (byte)0;
            var r = (byte)(b5bit * byte.MaxValue / 31);
            var g = (byte)(g5bit * byte.MaxValue / 31);

            SetColor(dst, index * 4, a, r, g, b);
        }

        return dst;
    }

    public static byte[] Ai88ToArgb32(byte[] src)
    {
        var dst = new byte[src.Length * 2];
        var nPixel = src.Length / 2;
        for (var index = 0; index < nPixel; ++index)
        {
            var grey = src[index * 2];
            var alpha = src[index * 2 + 1];

            SetColor(dst, index * 4, alpha, grey, grey, grey);
        }

        return dst;
    }

    public static byte[] P8ToARGB32(byte[] src, Palette palette)
    {
        var dst = new byte[src.Length * 4];
        var colors = palette.Colors;
        var nPixel = src.Length;
        for (var index = 0; index < nPixel; ++index)
        {
            var color = colors[src[index]];
            SetColor(dst, index * 4, color.Alpha, color.Red, color.Green, color.Blue);
        }

        return dst;
    }

    internal static byte[] ConvertToARGB32(byte[] data, int width, int height, PaaType type)
    {
        switch (type)
        {
            case PaaType.DXT1:
            case PaaType.DXT2:
            case PaaType.DXT3:
            case PaaType.DXT4:
            case PaaType.DXT5:
                return DXTDecoder.DxtToArgb32(data, width, height, type);
            case PaaType.RGBA_5551:
                return Argb1555ToArgb32(data);
            case PaaType.RGBA_4444:
                return Argb16ToArgb32(data);
            case PaaType.AI88:
                return Ai88ToArgb32(data);
            case PaaType.RGBA_8888:
                return data;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
}
