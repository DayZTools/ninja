﻿namespace Ninja.Lib.PAA;

public class Palette
{
    public List<PaaColor> Colors { get; private set; }

    public void Read(BinaryReader br)
    {
        var nPaletteTriplets = br.ReadUInt16();
        Colors = new List<PaaColor>();
        for (var i = 0; i < nPaletteTriplets; ++i)
        {
            var b = br.ReadByte();
            var g = br.ReadByte();
            var r = br.ReadByte();
            Colors.Add(new PaaColor(r, g, b));
        }
    }

    public void Write(BinaryWriter bw)
    {
        bw.Write((ushort) Colors.Count);
        foreach (var color in Colors)
        {
            bw.Write(color.Blue);
            bw.Write(color.Green);
            bw.Write(color.Red);
        }
    }
}
