# Contributing

## Reporting errors
Things that will help to fix bug:
* Check if bug already submitted.
* A minimal mod to reproduce error.
* Additional Info:
  * Utility name
  * Utility version
  * OS and OS version
  * Net Runtime version

## Development environment

### Required
* LTS Net SDK
* Java to build grammars

### Building
*Note: Make sure that all [Requirements](#Required) are satisfied.*

To build NinjaUtils you can use any C# IDE or just call ``dotnet publish``

