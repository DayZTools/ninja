﻿namespace Ninja.Lib.Pbo;

/// <summary>
/// Mime Type used for data
/// </summary>
public enum MimeType
{
    Uncompressed = 0x00000000,
    Compressed = 0x43707273,
    Header = 0x56657273,
}
