﻿using System.IO.Compression;
using System.Security.Cryptography;
using Ninja.Lib.Compression;
using Ninja.Lib.Compression.Lzss;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Pbo.IO;

/// <summary>
/// Writer stream for a PBO
/// </summary>
public sealed class PboWriter : BinaryWriter
{
    private readonly PboClient _client;

    public PboWriter(Stream stream, PboClient client)
        : base(stream)
    {
        _client = client;
    }

    /// <summary>
    /// File list to include in compression
    /// </summary>
    public string[] CompressList { get; set; } = Array.Empty<string>();

    public Dictionary<string, string> UserProperties { get; set; } = new();

    /// <summary>
    /// Write the checksum of the file
    /// </summary>
    public void WriteChecksum()
    {
        BaseStream.Seek(0, SeekOrigin.Begin);

        var sha = SHA1.Create();
        var sha1Bytes = sha.ComputeHash(BaseStream);

        base.Write((byte)0);
        base.Write(sha1Bytes);
    }

    /// <summary>
    /// Takes content of a folder and packs it in to PBO format
    /// </summary>
    /// 
    /// <param name="folder"></param>
    public void WritePbo(string folder)
    {
        try
        {
            _client.PushOnEvent("Starting WritePbo", EventType.Debug);

            // Write signature 
            WriteFileHeader(new PboFile { MimeType = MimeType.Header });

            WriteProperties();

            // Look for all the files 
            var files = PrepareFiles(folder);

            // Write all file structs 
            foreach (var file in files)
            {
                WriteFileHeader(file);
            }

            // Write final empty struct (terminator)
            WriteFileHeader(new PboFile { MimeType = MimeType.Uncompressed });

            // Write the data for each file 
            foreach (var file in files)
            {
                if (file.MimeType is MimeType.Uncompressed or MimeType.Compressed)
                    WriteFile(folder, file);
                else
                    throw new Exception("Unsupported packing method = " + file.MimeType);
            }

            _client.PushOnEvent("Finished WritePBO", EventType.Debug);
        }
        catch (Exception e)
        {
            _client.PushOnEvent($"WritePBO failed\n{e.Message}", EventType.Error);
        }
    }

    private List<PboFile> PrepareFiles(string folder)
    {
        var files = new List<PboFile>();

        foreach (var file in Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories))
        {
            var fileInfo = new FileInfo(file);

            // Make sure we are not reading the prefix ($) file
            if (fileInfo.Name[0] == 36 || fileInfo.Name[^1] == 36) continue;

            var mimeType = CompressList?
                .Select(pattern => new Wildcard(pattern))
                .Any(wp => wp.IsMatch(file)) == true
                ? MimeType.Compressed
                : MimeType.Uncompressed;

            files.Add(new PboFile
            {
                FileName = Path.GetRelativePath(folder, fileInfo.FullName),
                MimeType = mimeType,
                OriginalSize = (int)fileInfo.Length,
                Offset = 0,
                Timestamp = (int)(fileInfo.LastWriteTime - new DateTime(1970, 1, 1)).TotalSeconds,
                DataSize = 0, // will be overwritten later
            });
            _client.PushOnEvent($"Found File {fileInfo.FullName}", EventType.Debug);
        }

        return files;
    }

    private void WriteFileHeader(PboFile file)
    {
        this.WriteCString(file.FileName);
        Write((int)file.MimeType);
        Write(file.OriginalSize);
        file.OffsetToOffset = BaseStream.Position; // used to overwrite DataOffset and DataSize
        Write(file.Offset);
        Write(file.Timestamp);
        Write(file.DataSize);

        _client.PushOnEvent($"Wrote Header for: {file.FileName}", EventType.Info);
    }

    private void WriteProperties()
    {
        var assName = System.Reflection.Assembly.GetExecutingAssembly().GetName();
        var name = assName.Name;
        var version = assName.Version?.ToString();

        foreach (var (key, value) in UserProperties)
        {
            // disallow overwriting packer info
            if (key.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                continue;
            WriteProperty(new KeyValuePair<string, string>(key.ToLower(), value));
        }

        // Write packer info
        WriteProperty(new KeyValuePair<string, string>(name!, version ?? "unknown-version"));

        // Empty prop (terminator)
        Write((byte)0);
    }

    private void WriteProperty(KeyValuePair<string, string> property)
    {
        this.WriteCString(property.Key);
        this.WriteCString(property.Value);
    }

    private void WriteFile(string folder, PboFile file)
    {
        using var fs = File.OpenRead(Path.Combine(folder, file.FileName));

        file.Offset = (int)BaseStream.Position;
        if (file.MimeType == MimeType.Uncompressed)
        {
            var br = new BinaryReader(fs);
            Write(br.ReadBytes((int)fs.Length));
            file.DataSize = file.OriginalSize;
        }
        else
        {
            var size = 0;
            try
            {
                var pos = BaseStream.Position;
                Compressor.Compress(fs, BaseStream);
                size = (int)(BaseStream.Position - pos);
            }
            catch (Exception e)
            {
                _client.PushOnEvent($"Failed compress file \"{file.FileName}\" {e.Message}", EventType.Error);
                throw;
            }

            file.DataSize = size;
        }

        FixOffsetAndDataSize(file);

        _client.PushOnEvent($"Wrote Data for: {file.FileName}", EventType.Info);
    }

    private void FixOffsetAndDataSize(PboFile file)
    {
        var offReal = BaseStream.Position;
        BaseStream.Seek(file.OffsetToOffset, SeekOrigin.Begin);
        Write(file.Offset);
        Write(file.Timestamp);
        Write(file.DataSize);
        var off = file.Offset + file.DataSize;
        BaseStream.Seek(off, SeekOrigin.Begin);
    }
}
