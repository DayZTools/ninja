﻿using Ninja.Lib.Compression;
using Ninja.Lib.Compression.Lzss;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Pbo.IO;

/// <summary>
/// Reader stream for a PBO
/// </summary>
public sealed class PboReader : BinaryReader
{
    private readonly PboClient _client;

    public PboReader(Stream stream, PboClient client)
        : base(stream)
    {
        _client = client;
    }

    /// <summary>
    /// Keeps reading bytes until it encounters 0 then returns as string
    /// </summary>
    /// <returns></returns>
    public override string ReadString()
    {
        return this.ReadCString();
    }


    /// <summary>
    /// Read the SHA1 checksum of a PBO if it exists
    /// </summary>
    /// 
    /// <param name="pbo">PBO to read</param>
    public byte[] ReadChecksum()
    {
        BaseStream.Seek(BaseStream.Length - 21, SeekOrigin.Begin);

        // Read the end of the file
        var buffer = new byte[21];
        BaseStream.Read(buffer, 0, 21);
        // Reset the position at 0
        BaseStream.Seek(0, SeekOrigin.Begin);

        if (buffer[0] == 0)
        {
            var checksum = new byte[20];
            Array.Copy(buffer, 1, checksum, 0, 20);

            return checksum;
        }

        return new byte[20];
    }

    /// <summary>
    /// Reader through header structs 
    /// </summary>
    /// 
    /// <returns>PBOFile structure</returns>
    public PboFile ReadFileHeader()
    {
        var fileName = ReadString();
        var mimeType = (MimeType)ReadInt32();
        var originalSize = ReadInt32();
        var dataOffset = ReadInt32();
        var timeStamp = ReadInt32();
        var datasize = ReadInt32();

        return new PboFile
        {
            FileName = fileName,
            MimeType = mimeType,
            OriginalSize = originalSize,
            Offset = dataOffset,
            Timestamp = timeStamp,
            DataSize = datasize
        };
    }

    /// <summary>
    /// Read the header of the PBO
    /// </summary>
    /// 
    /// <param name="pbo">PBO to read</param>
    public void ReadHeader(Pbo pbo)
    {
        try
        {
            _client.PushOnEvent($"Starting Header Read for {pbo.FilePath}", EventType.Debug);

            // Get the signature
            var sig = ReadFileHeader();

            _client.PushOnEvent($"Signature read Packing Method: {sig.MimeType}", EventType.Debug);

            // Look for a properties 
            if (sig.MimeType == MimeType.Header)
            {
                pbo.Properties = new Dictionary<string, string>();

                while (true)
                {
                    var property = ReadString();
                    if (property == string.Empty) break;
                    pbo.Properties.Add(property, ReadString());
                }
            }

            // Read all file headers  
            while (true)
            {
                var file = ReadFileHeader();
                if (file.FileName == string.Empty) break;
                pbo.Files.Add(file);
                _client.PushOnEvent($"File found: {file.FileName}", EventType.Info);
            }

            // While Offset is optional, update corrupted file offsets
            foreach (var pboFile in pbo.Files)
            {
                pboFile.Offset = (int) BaseStream.Position;
                BaseStream.Position += pboFile.DataSize;
            }

            _client.PushOnEvent($"Header read successfully from {pbo.FilePath}", EventType.Debug);
        }
        catch (Exception ex)
        {
            _client.PushOnEvent($"Failed to read header for {pbo.FilePath}\n {ex.Message}", EventType.Error);
        }
    }

    /// <summary>
    /// Extract specific file data and returns it as a byte[]
    /// </summary>
    /// 
    /// <param name="file">PBO file to extract data</param>
    /// <param name="decompress">Decompress file while extract</param>
    /// <returns>Extracted bytes from the PBO file</returns>
    public byte[] ExtractFileData(PboFile file, bool decompress = true)
    {
        BaseStream.Seek(file.Offset, SeekOrigin.Begin);
        if (file.MimeType == MimeType.Compressed && decompress)
        {
            Decompressor.Decompress(BaseStream, out var data, file.DataSize, false);
            return data;
        }

        return ReadBytes(file.DataSize);
    }
}
