﻿using System.Text;
using Ninja.Lib.Pbo.IO;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Pbo;

/// <summary>
/// Implementation of reader and writer class to deal with PBO
/// </summary>
public class PboClient : IDisposable
{
    public event NinjaEventHandler? OnEvent;


    private FileStream? _fileReader;
    public PboReader? Reader { get; private set; } = null;


    /// <summary>
    /// Push event to event handler 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="type"></param>
    public void PushOnEvent(string message, EventType type) => OnEvent?.Invoke(new NinjaEventArgs(message, type));

    public void OpenPbo(string pboFilePath)
    {
        if (!pboFilePath.EndsWith(".pbo"))
            pboFilePath = $"{pboFilePath}.pbo";
        // Open FileStream
        _fileReader = new FileStream(pboFilePath, FileMode.Open, FileAccess.Read);
        // Create new instance of PBOReader
        Reader = new PboReader(_fileReader, this);
    }

    /// <summary>
    /// Analyzes the contents of the PBO File and returns it as a PBO object
    /// </summary>
    public Pbo? AnalyzePBO(string pboFilePath)
    {
        try
        {
            PushOnEvent("AnalyzePBO Started", EventType.Debug);

            if (Reader == null)
                OpenPbo(pboFilePath);

            // Create A PBO object to store all the PBO's attributes
            var pbo = new Pbo
            {
                FilePath = pboFilePath,
                Checksum = Reader!.ReadChecksum(),
            };

            Reader.ReadHeader(pbo);

            PushOnEvent("AnalyzePBO Finished", EventType.Debug);

            return pbo;
        }
        catch (Exception ex)
        {
            PushOnEvent(ex.Message, EventType.Error);
            return null;
        }
    }

    /// <summary>
    /// Extract all files from PBO.
    /// Will extract to extractionDir if specified.
    /// If not will output in the current Dir of the PBO
    /// </summary>
    /// 
    /// <param name="pbo">Analyzed PBO</param>
    /// <param name="extractionDir">Destination folder</param>
    public bool ExtractAll(Pbo pbo, string? extractionDir = null)
    {
        try
        {
            PushOnEvent($"Starting full extraction of {pbo.FilePath}", EventType.Debug);

            if (Reader == null)
                OpenPbo(pbo.FilePath);

            // Get output dir depending on if extractionDir was passed 
            var outputDir = extractionDir ?? Path.GetDirectoryName(pbo.FilePath)!;

            foreach (var prop in pbo.Properties)
            {
                WriteToFile($"{outputDir}\\${prop.Key.ToUpper()}$", Encoding.ASCII.GetBytes(prop.Value));
            }

            // Run through each file in the PBO and write to disk
            foreach (var pboFile in pbo.Files)
            {
                var ok = WriteToFile(Path.Combine(outputDir, pboFile.FileName), Reader!.ExtractFileData(pboFile));
                if (!ok) break;
            }

            PushOnEvent($"Successfully extracted {pbo.FilePath}", EventType.Debug);
        }
        catch (Exception ex)
        {
            PushOnEvent($"Failed to extract {pbo.FilePath}\n{ex.Message}", EventType.Error);
            return false;
        }

        return true;
    }

    /// <summary>
    /// Extract all files from PBO.
    /// Will extract to extractionDir if specified.
    /// If not will output in the current Dir of the PBO
    /// </summary>
    /// 
    /// <param name="pboFilePath">File path to the PBO</param>
    /// <param name="extractionDir">Destination folder</param>
    public void ExtractAll(string pboFilePath, string? extractionDir = null)
    {
        if (!File.Exists(pboFilePath))
        {
            PushOnEvent($"File {pboFilePath} does not exist", EventType.Error);
            return;
        }

        var pbo = AnalyzePBO(pboFilePath);

        if (pbo != null)
            ExtractAll(pbo, extractionDir);
        else
        {
            PushOnEvent($"Cannot read {pboFilePath}", EventType.Error);
        }
    }

    /// <summary>
    /// Packs specified folder in to PBO format.
    /// Will output to the parent directory unless packDirectory is specified.
    /// File name will be (folderName.pbo) unless pboName is specified.
    /// </summary>
    /// 
    /// <param name="folder">Folder to pack the PBO</param>
    /// <param name="pboFilePath">Output file path</param>
    /// <param name="pboName">PBO file name</param>
    /// <param name="compressList">List of files (wildcard) to compress</param>
    /// <param name="properties">Properties to add. "prefix" for e.g.</param>
    public bool PackPBO(string folder,
        string pboFilePath,
        string? pboName = null,
        string[]? compressList = null,
        Dictionary<string, string>? properties = null)
    {
        try
        {
            PushOnEvent("Starting PackPBO", EventType.Debug);

            // Get the correct pbo name depending on if pboName was passed 
            pboName ??= new DirectoryInfo(folder).Name;

            // Make sure the pboName has the correct extension
            if (new FileInfo(pboName).Extension != ".pbo")
                pboName = $"{pboName}.pbo";

            // Get the final pbo file path
            if (!pboFilePath.EndsWith(".pbo", StringComparison.InvariantCultureIgnoreCase))
                pboFilePath = Path.Combine(pboFilePath, "addons", pboName);


            if (File.Exists(pboFilePath))
            {
                File.Delete(pboFilePath);
            }

            // Create dir if it does not exist 
            if (!Directory.Exists(Path.GetDirectoryName(pboFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(pboFilePath)!);

            // Create the pbo file 
            using var fileStream = File.Create(pboFilePath);
            // Write the file content
            using var writer = new PboWriter(fileStream, this);
            if (compressList != null)
                writer.CompressList = compressList;

            if (properties != null)
                writer.UserProperties = properties;

            writer.WritePbo(folder);
            writer.WriteChecksum();

            PushOnEvent("Finished PackPBO", EventType.Debug);
        }
        catch (Exception ex)
        {
            PushOnEvent(ex.Message, EventType.Error);
            return false;
        }

        return true;
    }

    public void Dispose()
    {
        if (_fileReader == null) return;
        _fileReader.Close();
        _fileReader.Dispose();
    }

    private bool WriteToFile(string filePath, byte[] data)
    {
        try
        {
            // Create dir if needed
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filePath)!);

            // Write the data on the file
            File.WriteAllBytes(filePath, data);

            PushOnEvent($"{Path.GetFileName(filePath)} -> {filePath}", EventType.Info);
        }
        catch (Exception ex)
        {
            PushOnEvent($"Failed to Write {Path.GetFileName(filePath)} to disk\n{ex.Message}", EventType.Error);
            return false;
        }

        return true;
    }
}
