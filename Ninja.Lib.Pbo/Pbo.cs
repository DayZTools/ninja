﻿namespace Ninja.Lib.Pbo;

public class Pbo
{
    /// <summary>
    /// SHA1 checksum of the PBO
    /// </summary>
    public byte[] Checksum { get; internal set; } = null!;

    /// <summary>
    /// List of the PBO file
    /// </summary>
    public List<PboFile> Files { get; internal set; } = new();

    /// <summary>
    /// File path of the PBO
    /// </summary>
    public string FilePath { get; internal set; } = null!;

    /// <summary>
    /// PBO properties
    /// </summary>
    public Dictionary<string, string> Properties { get; internal set; } = null!;
}
