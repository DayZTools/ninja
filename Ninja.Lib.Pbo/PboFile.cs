﻿namespace Ninja.Lib.Pbo;

public class PboFile
{
    /// <summary>
    /// Inner file path in the PBO
    /// </summary>
    public string FileName { get; internal init; } = string.Empty;

    /// <summary>
    /// Packing method in the PBO
    /// </summary>
    public MimeType MimeType { get; internal init; }

    /// <summary>
    /// Original size of the file: uncompressed final size
    /// </summary>
    public int OriginalSize { get; internal init; } = 0;

    /// <summary>
    /// Offset to the file data
    /// </summary>
    public int Offset { get; internal set; } = 0;

    /// <summary>
    /// Timestamp of the file
    /// </summary>
    public int Timestamp { get; internal init; } = 0;

    /// <summary>
    /// Data size in the PBO: can be compressed
    /// </summary>
    public int DataSize { get; internal set; } = 0;

    internal long OffsetToOffset { get; set; } = 0;
}
