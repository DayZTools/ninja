﻿using System.Text.Json;
using System.Text.Json.Serialization;
using NinjaMake;
using NinjaMake.Models;

var makeFile = args.Length == 0 ? Directory.GetCurrentDirectory() : args[0];

if (!makeFile.EndsWith(".json"))
{
    if (makeFile[^1] != '\\')
        makeFile += "\\";
    makeFile += "makefile.json";
}

if (!File.Exists(makeFile))
{
    Console.WriteLine($"{makeFile} not found.");
    return 1;
}

var file = File.ReadAllText(makeFile);
Config config;

try
{
    config = JsonSerializer.Deserialize<Config>(file, new JsonSerializerOptions
    {
        Converters =
        {
            new JsonStringEnumConverter()
        }
    })!;
}
catch (Exception e)
{
    var fname = new FileInfo(makeFile).Name;
    Console.WriteLine($"Error in {fname}: " + e.Message);
    return 1;
}

if (config.Targets == null)
{
    Console.WriteLine("Error: makefile should contain at least 1 target to build");
    return 1;
}

var wd = Directory.GetParent(makeFile)?.FullName;

var builder = new Builder(config, wd!);
foreach (var target in config.Targets)
{
    try
    {
        builder.Build(target);
    }
    catch (Exception e)
    {
        Console.WriteLine($"{e.ToString()}");
        return 1;
    }
}

return 0;
