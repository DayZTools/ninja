﻿using System.Management.Automation;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Ninja.Lib.Cpp;
using Ninja.Lib.DSKeys;
using Ninja.Lib.Pbo;
using Ninja.Lib.Utils;
using NinjaMake.Models;

namespace NinjaMake;

public class Builder
{
    private Config _config;

    public string WorkingDirectory { get; set; } = null!;

    public Builder(Config config, string workingDirectory)
    {
        _config = config;
        SetWorkingDirectory(workingDirectory);
    }

    public void SetWorkingDirectory(string workingDirectory)
    {
        if (string.IsNullOrEmpty(workingDirectory))
        {
           throw new ArgumentException("Error: Cannot set working directory", nameof(workingDirectory));
        }

        Directory.SetCurrentDirectory(workingDirectory);
        WorkingDirectory = workingDirectory;
    }

    public void Build(BuildUnit unit)
    {
        unit.Init(WorkingDirectory, _config);
        
        Console.Write($"Making {unit.Name}");
        
        foreach (var bs in unit.BuildScripts!.Where(bs => bs.Type is BuildScriptType.BeforeBuild))
        {
            unit.GeneratedFiles.AddRange(ExecuteScript(unit, bs.Path, "BeforeBuild"));
        }

        if (IsFilesUpToDate(unit))
        {
            Console.WriteLine($" is up to date. Skipping...");
            return;
        }
        else
        {
            Console.WriteLine();
        }

        if (!File.Exists(Path.Join(unit.SourcePath, "config.cpp")))
        {
            throw new BuildUnitException(unit, "config.cpp not found");
        }

        StageCopyFiles(unit);
        StageBinarizeConfigs(unit);
        StageBinarizeResources(unit);
        
        foreach (var bs in unit.BuildScripts!.Where(bs => bs.Type is BuildScriptType.BeforePack))
        {
            ExecuteScript(unit, bs.Path, "BeforePack");
        }
        
        StagePackPbo(unit);

        if (unit.Sign == true)
        {
            StageSignPbo(unit);
        }

        if (unit.CopyKey == true)
        {
            StageCopyKey(unit);
        }
        
        foreach (var bs in unit.BuildScripts!.Where(bs => bs.Type is BuildScriptType.AfterBuild))
        {
            ExecuteScript(unit, bs.Path, "AfterBuild");
        }
    }

    private List<string> ExecuteScript(BuildUnit unit, string path, string method)
    {
        var generatedFiles = new List<string>();
        using var ps = PowerShell.Create();
        var script = File.ReadAllText(path);
        ps.AddScript(script).AddParameters(new object?[] { unit, generatedFiles, this });
        try
        {
            var results = ps.Invoke();
            foreach (var psObject in results)
            {
                Console.WriteLine(psObject);
            }

            var sb = new StringBuilder();
            foreach (var error in ps.Streams.Error)
            {
                sb.Append(error);
            }

            if (sb.Length != 0)
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception e)
        {
            throw new BuildUnitException(unit, "powershell script had errors", e);
        }

        return generatedFiles;
    }

    private void StageCopyFiles(BuildUnit unit)
    {
        if (Directory.Exists(unit.StagePath))
            Directory.Delete(unit.StagePath, true);
        Directory.CreateDirectory(unit.StagePath!);
        var allFiles = Directory.GetFiles(unit.SourcePath!, "*.*", SearchOption.AllDirectories);

        var excludeRegexps = unit.ExclusionList?.Select(s => new Wildcard(s)).ToList();

        foreach (var file in allFiles)
        {
            var isInExcludeList = excludeRegexps?.Any(re => re.IsMatch(Path.GetFileName(file))) ?? false;

            if (isInExcludeList)
                continue;


            var fileInfo = new FileInfo(file);

            // skip properties
            if (fileInfo.Name[0] == 36 || fileInfo.Name[^1] == 36)
            {
                continue;
            }

            // skip files that would be binarized
            if (fileInfo.Name.EndsWith(".cpp", StringComparison.InvariantCultureIgnoreCase)
                || fileInfo.Name.EndsWith(".rvmat", StringComparison.InvariantCultureIgnoreCase))
            {
                continue;
            }

            var rpath = Path.GetRelativePath(unit.SourcePath!, fileInfo.FullName);
            var destPath = Path.Join(unit.StagePath, rpath);

            Directory.CreateDirectory(destPath[..^fileInfo.Name.Length]);
            File.Copy(file, destPath);
        }
    }

    private void StageBinarizeConfigs(BuildUnit unit)
    {
        var toBinFiles = Directory.GetFiles(unit.SourcePath!, "*.cpp", SearchOption.AllDirectories).ToList();
        toBinFiles.AddRange(Directory.GetFiles(unit.SourcePath!, "*.rvmat", SearchOption.AllDirectories));

        foreach (var file in toBinFiles)
        {
            var fileInfo = new FileInfo(file);

            var isCpp = file.EndsWith(".cpp");

            var targetExt = ".bin";

            if (!isCpp)
                targetExt = ".rvmat";

            var rpath = Path.GetRelativePath(unit.SourcePath!, fileInfo.FullName);
            rpath = Path.Join(Path.GetDirectoryName(rpath), fileInfo.Name[..^fileInfo.Extension.Length] + targetExt);
            var destPath = Path.Join(unit.StagePath, rpath);
            Directory.CreateDirectory(destPath[..^fileInfo.Name.Length]);

            using var inStream = File.OpenRead(file);
            using var outStream = File.Create(destPath);

            // todo: fix paths according to prefix
            Console.WriteLine("TODO BinarizeConfigs: fix paths according to prefix");
            var rf = Rapify.GetRapFile(inStream, isCpp);
            rf.Write(new BinaryWriter(outStream));
        }
    }

    private void StageBinarizeResources(BuildUnit unit)
    {
        Console.WriteLine("TODO BinarizeResources: binarize models, fix paths in textures according to prefix");
    }

    private void StagePackPbo(BuildUnit unit)
    {
        var fullStagePath = Path.GetFullPath(unit.StagePath!);
        var fullOutputPath = Path.GetFullPath(unit.OutputPath!);
        var pboClient = new PboClient();
        pboClient.OnEvent += PboClientOnOnEvent;

        pboClient.PackPBO(fullStagePath, fullOutputPath, unit.PboName, unit.CompressList, unit.Properties);
    }

    private void StageSignPbo(BuildUnit unit)
    {
        if (!File.Exists(unit.PrivateKey))
        {
            CreateKey(unit);
        }

        var pboClient = new PboClient();
        pboClient.OnEvent += PboClientOnOnEvent;

        var outPath = Path.GetFullPath(unit.OutputPath!);

        var pbo = pboClient.AnalyzePBO(Path.Join(outPath, "addons", unit.PboName));

        if (pbo == null)
        {
            throw new BuildUnitException(unit, "Failed to analyze produced PBO file");
        }

        var pboSign = new PboSign(pbo, pboClient);

        var biPrivate = BiPrivate.ReadKey(unit.PrivateKey!);

        var biSign = pboSign.Sign(biPrivate);
        var signPath = Path.Join(outPath, "addons", $"{unit.PboName}.pbo.{biSign.Name}.bisign");
        biSign.WriteKey(signPath);
    }

    private void CreateKey(BuildUnit unit)
    {
        string? answer;
        if (string.IsNullOrEmpty(unit.PrivateKey)) goto exit;
        while (true)
        {
            Console.Write("Private key is not exists, generate it (yes/no):");
            answer = Console.ReadLine()?.ToLower();
            if (answer is "yes" or "no")
                break;
            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        }

        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);

        if (answer == "no") goto exit;
        Console.WriteLine($"Generating key: {unit.PrivateKey}");

        var name = Path.GetFileName(unit.PrivateKey)!;
        name = name[..name.LastIndexOf('.')];
        var biPrivate = BiPrivate.Generate(name);
        FileStream? fs = null;
        try
        {
            fs = File.Create(unit.PrivateKey!);
            biPrivate.WriteKey(fs);
        }
        finally
        {
            fs?.Dispose();
            fs = null;
        }

        try
        {
            fs = File.Create(name + ".bikey");
            var biPublic = BiPublic.Generate(biPrivate);
            biPublic.WriteKey(fs);
        }
        finally
        {
            fs?.Dispose();
        }

        return;
        exit:
        throw new BuildUnitException(unit, "PrivateKey not found");
    }

    private void StageCopyKey(BuildUnit unit)
    {
        var keyPath = Path.GetDirectoryName(unit.PrivateKey);
        var keyName = new FileInfo(unit.PrivateKey!).Name;
        keyName = keyName[..keyName.LastIndexOf(".", StringComparison.InvariantCulture)] + ".bikey";
        keyPath = Path.Join(keyPath, keyName);
        if (!File.Exists(keyPath))
        {
            throw new BuildUnitException(unit, "BiKey file not found");
        }

        var destPath = Path.Join(unit.OutputPath, "keys", keyName);
        if (File.Exists(destPath))
            File.Delete(destPath);

        if (!Directory.Exists(destPath))
            Directory.CreateDirectory(Path.GetDirectoryName(destPath));

        File.Copy(keyPath, destPath);
    }

    private void PboClientOnOnEvent(NinjaEventArgs args)
    {
        if (args.Type != EventType.Error) return;
        Console.WriteLine($"{args.Type}: {args.Message}");
    }

    private bool IsFilesUpToDate(BuildUnit unit)
    {
        var newHashes = GenerateFileHashes(unit);
        if (!File.Exists(GetHashPath(unit)))
        {
            goto updated;
        }

        var oldHashes = ReadFileHashes(unit);
        if (oldHashes != null && oldHashes.Count == newHashes.Count && !oldHashes.Except(newHashes).Any())
        {
            return true;
        }

        updated:
        WriteFileHashes(unit, newHashes);
        return false;
    }

    private static Dictionary<string, string> GenerateFileHashes(BuildUnit unit)
    {
        var dict = Directory.EnumerateFiles(unit.SourcePath,
            "*.*",
            SearchOption.AllDirectories
        ).ToDictionary(file => file, GetFileChecksum);
        
        foreach (var file in unit.GeneratedFiles)
        {
            dict.Add(file, GetFileChecksum(file));
        }

        using var sha256 = SHA256.Create();
        var json = JsonSerializer.Serialize(unit);
        var hash = Convert.ToBase64String(sha256.ComputeHash(Encoding.UTF8.GetBytes(json)));
        dict.Add("%BuildUnit%", hash);

        return dict;
    }

    private static Dictionary<string, string>? ReadFileHashes(BuildUnit unit)
    {
        var file = File.ReadAllText(GetHashPath(unit));
        return JsonSerializer.Deserialize<Dictionary<string, string>>(file);
    }

    private static void WriteFileHashes(BuildUnit unit, Dictionary<string, string> hashes)
    {
        var json = JsonSerializer.Serialize(hashes);
        File.WriteAllText(GetHashPath(unit), json);
    }

    private static string GetHashPath(BuildUnit unit)
    {
        return Path.Join(Directory.GetParent(unit.StagePath).FullName, unit.Name + "_hashes.json");
    }

    private static string GetFileChecksum(string filePath)
    {
        using var sha256 = SHA256.Create();
        using var fileStream = File.OpenRead(filePath);
        return Convert.ToBase64String(sha256.ComputeHash(fileStream));
    }
}
