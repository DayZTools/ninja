﻿using System.Text.Json.Serialization;
using Ninja.Lib.Utils;
using NinjaMake.Models;

namespace NinjaMake;

public static class BuildUnitExt
{
    public static void PrintWarning(this BuildUnit unit, string message)
    {
        Console.WriteLine("Target \"" + unit.Name + "\": " + message);
    }

    private static void InitFromGlobalTarget(this BuildUnit unit, string workingDirectory, TargetGlobalSettings? globalTarget)
    {
        if (globalTarget == null) return;
        
        unit.OutputPath ??= globalTarget.OutputPath;
        unit.PrivateKey ??= globalTarget.PrivateKey;

        unit.Sign ??= globalTarget.Sign ?? !string.IsNullOrEmpty(unit.PrivateKey);
        unit.CopyKey ??= globalTarget.CopyKey ?? !string.IsNullOrEmpty(unit.PrivateKey);

        var sp = workingDirectory;
        if (!string.IsNullOrEmpty(globalTarget.SourceRootPath))
        {
            sp = globalTarget.SourceRootPath;
        }

        unit.SourcePath ??= Path.Join(sp, unit.Name);
        unit.StagePath ??= globalTarget.StagePath;

        unit.ExclusionList ??= globalTarget.ExclusionList;
        unit.CompressList ??= globalTarget.CompressList;
        
        unit.PrefixRoot ??= globalTarget.PrefixRoot;

        if (globalTarget.BuildScripts != null)
        {
            unit.BuildScripts?.AddRange(globalTarget.BuildScripts);
        }
    }

    public static void Init(this BuildUnit unit, string workingDirectory, Config config)
    {
        if (string.IsNullOrEmpty(unit.Name))
        {
            throw new Exception("Target must contain Name property");
        }

        if (string.IsNullOrEmpty(unit.Type))
        {
            throw new BuildUnitException(unit, "must contain Type property");
        }

        unit.BuildScripts ??= new List<BuildScript>();
        
        var globalTarget =
            config.Default?.Targets
                ?.Where(x => string.Equals(x.Key, unit.Type, StringComparison.OrdinalIgnoreCase))
                .Select(x => x.Value).FirstOrDefault();

        InitFromGlobalTarget(unit, workingDirectory, globalTarget);

        if (string.IsNullOrEmpty(unit.SourcePath))
        {
            var sp = workingDirectory;
            if (!string.IsNullOrEmpty(config.Default?.SourceRootPath))
            {
                sp = config.Default.SourceRootPath;
            }

            unit.SourcePath ??= Path.Join(sp, unit.Name);
        }

        const string errStr = "{1} must be declared in Default.Targets.{0} or in Target itself";
        if (string.IsNullOrEmpty(unit.OutputPath))
            throw new BuildUnitException(unit, string.Format(errStr, unit.Type, "OutputPath"));
        if (string.IsNullOrEmpty(unit.SourcePath))
            throw new BuildUnitException(unit, string.Format(errStr, unit.Type, "SourcePath"));

        unit.PrivateKey ??= config.Default?.PrivateKey;

        if (unit.PrivateKey == "")
            PrintWarning(unit, "PrivateKey is empty");

        unit.StagePath ??= config.Default?.StagePath;
        unit.StagePath ??= DirectoryExt.CreateTemporaryDirectory();

        unit.StagePath = Path.Join(unit.StagePath, unit.Name);

        if (unit.Properties == null)
        {
            unit.Properties = new Dictionary<string, string>();
        }
        else
        {
            var convertedProps = new Dictionary<string, string>();
            foreach (var (key, value) in unit.Properties)
            {
                convertedProps.Add(key.ToLower(), value);
            }

            unit.Properties = convertedProps;
        }
        
        unit.PrefixRoot ??= config.Default?.PrefixRoot;

        if (unit.Prefix == null)
        {
            var relDir = workingDirectory;
            if (unit.PrefixRoot != null)
                relDir = unit.PrefixRoot;

            // todo: must check if SourcePath contains relDir
            unit.Prefix = Path.GetRelativePath(relDir, unit.SourcePath);
        }

        unit.Sign ??= false;
        unit.CopyKey ??= false;

        if (string.IsNullOrEmpty(unit.PboName))
        {
            unit.PboName = unit.Name;
        }
    }
}
