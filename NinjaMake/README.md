# NinjaMake

A utility that is designed to ease the build process of the DayZ mods.

## Features
* Binarizing ``*.cpp`` and ``*.rvmat`` files
* Packing PBOs with compression
* Signing PBOs
* Creating ``BiPrivate`` and ``BiPublic`` if required by user
* Scripting with PowerShell (BeforeBuild, AfterBuild, BeforePack events)
* Linux support (not tested)
* Changes tracking

## In Progress
* Binarizing textures

## Todo list
* Better build process logging
* Better error handling (CPP, Pbo, DSKeys)
* CPP: More validators
* ``makefile.json`` generator (CLI or GUI)

## Long term todo list
* Binarizing models
  * MLOD: Validating paths while build
  * ODOL: Validating paths while build
* Binarizing WRP (maps)
* Validating translated strings and warn when string constants where used in configs or strings
* Console version of BIS ``publish.exe``
* More tests

## Prerequisites
Every mod MUST satisfy the following file structure:
```
ModRoot
ModRoot\makefile.json
ModRoot\Mod1
...
ModRoot\ModN
```

## Makefile template

Most of properties in ``Root.Default.Targets.TargetName`` can be omitted.  
``Root.Targets[]`` can override properties from ``Root.Default.Targets.TargetName``.

```json
{
  "Default": {
    "PrivateKey": "C:\\ModRoot\\Keys\\MyKey.biprivatekey",
    "StagePath": ".\\Stage",
    "PrefixRoot": "C:\\ModRoot",
    "Targets": {
      "TargetName": {
        "OutputPath": ".\\Output",
        "Sign": true,
        "CopyKey": true,
        "CompressList": [ "*.c", "*.csv" ]
      }
    }
  },
  "Targets": [
    {
      "Name": "Mod1",
      "Type": "TargetName"
    }
  ]
}
```

## Build Scripts
Build scripts are PowerShell scripts that are executed when the specific event occurred.

Exists (at the moments) next types of scripts:
* BeforeBuild (called right after initializing Target)
* BeforePack (called after all build steps but before pack)
* AfterBuild (fired after all build steps)

### Script example
**TODO**
