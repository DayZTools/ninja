﻿using NinjaMake.Models;

namespace NinjaMake;

public class BuildUnitException : Exception
{
    public BuildUnitException()
    {
    }

    public BuildUnitException(BuildUnit unit, string message)
        : base(MakeMessage(unit, message))
    {
    }

    public BuildUnitException(BuildUnit unit, string message, Exception inner) : base(MakeMessage(unit, message), inner)
    {
    }

    private static string MakeMessage(BuildUnit unit, string message)
    {
        return "Target \"" + unit.Name + "\": " + message;
    }
}
