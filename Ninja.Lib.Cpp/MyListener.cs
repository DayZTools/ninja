﻿using System.Globalization;
using Ninja.Lib.Cpp.Exceptions;
using Ninja.Lib.Cpp.Writer;
using Array = Ninja.Lib.Cpp.Writer.Array;
using Enum = Ninja.Lib.Cpp.Writer.Enum;

internal class TreeClass : Class
{
    public TreeClass? ParentNode;
    public TreeClass? Extend;
}

internal class MyListener : CppParserBaseListener
{
    private TreeClass _curClass;
    private TreeClass _root;
    private Enum _enum; 

    public MyListener()
    {
        _root = new TreeClass();
        _enum = new Enum();
        _curClass = _root;
    }

    public RapFile GetFile()
    {
        var rapFile = new RapFile
        {
            EntryPoint = _root,
            Enum = _enum
        };
        return rapFile;
    }

    bool ValidateBaseClass(string name)
    {
        var c = _curClass.ParentNode ?? _root;

        if (c.Extend != null)
        {
            c = c.Extend;
        }

        return FindBaseClass(c, name);
    }

    static bool FindBaseClass(TreeClass? c, string name)
    {
        if (c == null)
            return false;
        if (c.Entries.Any(x => x.Name == name && (x is Class or ExternClass)))
            return true;
        return  FindBaseClass(c.ParentNode, name);
    }

    public override void EnterClassStatement(CppParser.ClassStatementContext context)
    {
        var parent = _curClass;
        _curClass = new TreeClass
        {
            Name = context.Identifier().GetText(),
            ParentNode = parent
        };

        var baseClass = context.extendClause()?.Identifier().GetText();
        if (baseClass != null)
        {
            if (ValidateBaseClass(baseClass) == false)
            {
                throw new RapifyException("class " + baseClass + " not declared");
            }

            _curClass.Inherited = baseClass;
            _curClass.Extend = _curClass.ParentNode?.Extend?.Entries.Find(x => x.Name == baseClass) as TreeClass
                               ?? _curClass.ParentNode?.Entries.Find(x => x.Name == baseClass) as TreeClass;
        }
    }

    public override void ExitClassStatement(CppParser.ClassStatementContext context)
    {
        _curClass.ParentNode!.Entries.Add(_curClass);
        _curClass = _curClass.ParentNode;
    }

    public override void EnterDeleteStatement(CppParser.DeleteStatementContext context)
    {
        _curClass.Entries.Add(new DeleteClass
        {
            Name = context.Identifier().GetText()
        });
    }

    public override void EnterExternStatement(CppParser.ExternStatementContext context)
    {
        var name = context.Identifier().GetText();
        _curClass.Entries.Add(new ExternClass
        {
            Name = name
        });
    }

    public override void EnterVariableExpression(CppParser.VariableExpressionContext context)
    {
        var variable = new Variable
        {
            Name = context.Identifier().GetText()
        };
        var expr = context.primaryExpression();

        variable.Value = SimpleLiteralParser(expr);
        _curClass.Entries.Add(variable);
    }

    private object SimpleLiteralParser(CppParser.PrimaryExpressionContext expr)
    {
        var type = expr.Start.Type;
        var str = expr.GetText();
        return type switch
        {
            CppLexer.StringLiteral => str.Substring(1, str.Length - 2), // exclude " symbols
            CppLexer.FloatingLiteral => float.Parse(str, new NumberFormatInfo { NumberDecimalSeparator = "." }),
            CppLexer.IntegerLiteral => int.Parse(str),
            _ => throw new RapifyException("(" + expr.Start.Line + ":" + expr.Start.Column 
                                           + ") Unknown expression type: " + expr.GetText())
        };
    }

    public override void EnterListExpression(CppParser.ListExpressionContext context)
    {
        var array = new Array
        {
            Append = false,
            Name = context.Identifier().GetText()
        };

        ParseList(context.initializerList(), array.Elements);

        _curClass.Entries.Add(array);
    }


    public override void EnterAppendListExpression(CppParser.AppendListExpressionContext context)
    {
        var array = new Array
        {
            Append = false,
            Name = context.Identifier().GetText()
        };

        ParseList(context.initializerList(), array.Elements);

        _curClass.Entries.Add(array);
    }


    private void ParseList(CppParser.InitializerListContext context, ICollection<object> list)
    {
        var values = context?.listValue();
        if (values == null) return;
        foreach (var val in values)
        {
            var expr = val.primaryExpression();
            if (expr != null)
            {
                list.Add(SimpleLiteralParser(expr));
            }
            else
            {
                var newList = new List<object>();
                ParseList(val.initializerList(), newList);
                list.Add(newList);
            }
        }
    }
}
