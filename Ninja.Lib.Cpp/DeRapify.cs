﻿using Ninja.Lib.Cpp.Reader;

namespace Ninja.Lib.Cpp;

public static class DeRapify
{
    public static void DeRapifyStream(BinaryReader stream, Stream outStream)
    {
        var rapFile = new RapFile();
        rapFile.Read(stream);

        rapFile.SaveTo(outStream);
    }
    
    public static void DeRapifyStream(Stream stream, Stream outStream)
    {
        DeRapifyStream(new BinaryReader(stream), outStream);
    }
}
