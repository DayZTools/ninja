﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public abstract class Entry
{
    public string Name { get; set; } = string.Empty;

    public virtual void BuildString(IndendedTextWriter stringBuilder)
    {
        throw new NotImplementedException();
    }
}
