﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class Enum : Entry
{
    public Dictionary<string, int> enumList { get; set; } = new();

    public override void BuildString(IndendedTextWriter stringBuilder)
    {
        if (enumList.Count == 0) return;
        stringBuilder.WriteIndendedLine("enum {");
        stringBuilder.UpIdent();
        foreach (var kvp in enumList)
        {
            stringBuilder.WriteIndended($"{kvp.Key} = {kvp.Value}");
            if (!enumList[kvp.Key].Equals(enumList.Last().Value))
            {
                stringBuilder.Write(",");
            }
            stringBuilder.WriteLine();
        }

        stringBuilder.DownIdent();
        stringBuilder.WriteIndendedLine("};");
        stringBuilder.WriteLine();
    }
}
