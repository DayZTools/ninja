﻿using System.Globalization;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class Variable : Entry
{
    public object? Value { get; set; }

    public override void BuildString(IndendedTextWriter stringBuilder)
    {
        switch (Value)
        {
            case string:
                stringBuilder.WriteIndendedLine($"{Name} = \"{Value}\";");
                break;
            case float flt:
            {
                var val = flt.ToString(new NumberFormatInfo
                {
                    NumberDecimalSeparator = "."
                });
            
                stringBuilder.WriteIndendedLine($"{Name} = {val};");
                break;
            }
            default:
                stringBuilder.WriteIndendedLine($"{Name} = {Value};");
                break;
        }
    }
}
