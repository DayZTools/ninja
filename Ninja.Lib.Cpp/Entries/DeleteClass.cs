﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class DeleteClass : Entry
{
    public override void BuildString(IndendedTextWriter stringBuilder)
    {
        stringBuilder.WriteIndendedLine($"delete {Name};");
    }
}
