﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class RapFile : Entry
{
    public Class EntryPoint { get; set; } = new();
    public Enum Enum { get; set; } = new();
    protected static readonly byte[] ValidSignature = new byte[4] { 0x0, 0x72, 0x61, 0x50 };
    protected static int HeaderLength = 16; // 4 byte sign, (int) 0, (int) 8, (uint) offsetToEnum == 16

    private void Build(IndendedTextWriter writer)
    {
        Enum.BuildString(writer);
        EntryPoint.BuildBody(writer);
    }

    public override string ToString()
    {
        using var buildStream = new MemoryIndendedTextWriter();

        Build(buildStream);

        return buildStream.ToString();
    }

    public void SaveTo(Stream stream)
    {
        var buildStream = new IndendedTextWriter(stream);
        Build(buildStream);
    }
}
