﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class ExternClass : Entry
{
    /*public override void Write(BinaryWriter stream)
    {
        stream.Write((byte) 3);
        stream.WriteCString(Name);
    }*/
    
    public override void BuildString(IndendedTextWriter stringBuilder)
    {
        stringBuilder.WriteIndendedLine($"class {Name};");
    }
}
