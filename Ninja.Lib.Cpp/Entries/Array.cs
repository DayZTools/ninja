﻿using System.Globalization;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class Array : Entry
{
    public bool Append { get; set; } = false;
    public List<object> Elements { get; set; } = new();

    public override void BuildString(IndendedTextWriter stringBuilder)
    {

        stringBuilder.WriteIndended($"{Name}[] = ");
        BuildArr(stringBuilder, Elements);
    }

    private void BuildArr(IndendedTextWriter stringBuilder, IReadOnlyList<object> arr)
    {
        stringBuilder.Write("{");
        stringBuilder.WriteLine();
        stringBuilder.UpIdent();

        for (var i = 0; i < arr.Count; i++)
        {
            var element = arr[i];
            switch (element)
            {
                case string str:
                    stringBuilder.WriteIndended($"\"{str}\"");
                    break;
                case float flt:
                    stringBuilder.WriteIndended(flt.ToString(new NumberFormatInfo
                    {
                        NumberDecimalSeparator = "."
                    }));
                    break;
                case int dcm:
                    stringBuilder.WriteIndended(dcm.ToString());
                    break;
                case List<object> lst:
                    stringBuilder.WriteIndent();
                    BuildArr(stringBuilder, lst);
                    break;
            }

            if (i != arr.Count - 1)
            {
                stringBuilder.Write(",");
            }
            stringBuilder.WriteLine();
        }

        stringBuilder.DownIdent();
        stringBuilder.WriteIndended("}");
        if (arr == Elements)
            stringBuilder.WriteLine(";");
    }
}
