﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Entries;

public class Class : Entry
{

    public string Inherited { get; set; } = string.Empty;
    public List<Entry> Entries { get; set; } = new();

    public override void BuildString(IndendedTextWriter stringBuilder)
    {
        stringBuilder.WriteIndended($"class {Name}");
        if (!string.IsNullOrEmpty(Inherited))
        {
            stringBuilder.Write($": {Inherited}");
        }

        stringBuilder.WriteLine(" {");
        stringBuilder.UpIdent();

        BuildBody(stringBuilder);

        stringBuilder.DownIdent();
        stringBuilder.WriteIndendedLine("};");
    }

    public void BuildBody(IndendedTextWriter stringBuilder)
    {
        foreach (var entry in Entries)
        {
            entry.BuildString(stringBuilder);
        }
    }
}
