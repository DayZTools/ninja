﻿namespace Ninja.Lib.Cpp.Exceptions;

public class DerapifyException: Exception
{
    public DerapifyException()
    {
    }

    public DerapifyException(string message)
        : base(message)
    {
    }

    public DerapifyException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
