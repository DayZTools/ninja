﻿namespace Ninja.Lib.Cpp.Exceptions;

public class RapifyException: Exception
{
    public RapifyException()
    {
    }

    public RapifyException(string message)
        : base(message)
    {
    }

    public RapifyException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
