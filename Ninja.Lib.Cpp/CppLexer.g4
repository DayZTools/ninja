lexer grammar CppLexer;

@header {#pragma warning disable 3021}

Semicolon: ';';
Colon: ':';
Coma: ',';
Plus: '+';
Minus: '-';
Assign: '=';
LBracket: '[';
RBracket: ']';
LBrace: '{';
RBrace: '}';
Class: 'class';
Delete: 'delete';

StringLiteral
    : '"' Schar* '"'
    ;
    
IntegerLiteral
    : Minus? (NONZERODIGIT) (DIGIT)*
    | '0'
    ;
    
FloatingLiteral
    : Minus? Fractionalconstant
    ;
    

Identifier
    :NONDIGIT (NONDIGIT | DIGIT)*;

fragment NONZERODIGIT: [1-9];
fragment NONDIGIT: [a-zA-Z_];
fragment DIGIT: [0-9];

fragment Schar:
	~ ["\\\r\n]
	| Escapesequence
	;
	
fragment Escapesequence
    : '\\\''
    | '\\'
    | '\\"'
    | '\\?'
    | '\\\\'
    | '\\a'
    | '\\b'
    | '\\f'
    | '\\n'
    | '\\r'
    | ('\\' ('\r' '\n'? | '\n'))
    | '\\t'
    | '\\v'
    ;

fragment Fractionalconstant
    : Digitsequence? '.' Digitsequence
	| Digitsequence '.'
	;
	
fragment Digitsequence: DIGIT+;

Whitespace: [ \t]+ -> skip;
Newline: ('\r' '\n'? | '\n') -> skip;
LineComment: '//' ~ [\r\n]* -> skip;
BlockComment: '/*' .*? '*/' -> skip;
