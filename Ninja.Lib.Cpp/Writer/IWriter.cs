﻿namespace Ninja.Lib.Cpp.Writer;

public interface IWriter
{
    protected static void WriteCompressedInt(BinaryWriter stream, int value)
    {
        unchecked
        {
            var v = (ulong) value;
            if (v == 0)
            {
                stream.Write((byte) 0);
            }

            while (v > 0)
            {
                if (v > 0x7f)
                {
                    stream.Write((byte) (0x80 | (v & 0x7f)));
                    v >>= 7;
                }
                else
                {
                    stream.Write((byte) v);
                    v = 0;
                }
            }
        }
    }

    void Write(BinaryWriter stream);
}
