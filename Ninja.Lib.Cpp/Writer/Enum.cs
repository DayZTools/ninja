﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class Enum: Entries.Enum, IWriter
{
    public void Write(BinaryWriter stream)
    {
        stream.Write(enumList.Count);
        foreach (var (key, value) in enumList)
        {
            stream.WriteCString(key);
            stream.Write(value);
        }
    }
}
