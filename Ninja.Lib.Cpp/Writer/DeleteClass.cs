﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class DeleteClass: Entries.DeleteClass, IWriter
{
    public void Write(BinaryWriter stream)
    {
        stream.Write((byte) 4);
        stream.WriteCString(Name);
    }
}
