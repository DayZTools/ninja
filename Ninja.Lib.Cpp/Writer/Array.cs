﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class Array: Entries.Array, IWriter
{
    public void Write(BinaryWriter stream)
    {
        // write array type
        if (Append) stream.Write((byte) 5);
        else stream.Write((byte) 2);
        stream.WriteCString(Name);
        WriteBody(stream, Elements);
    }

    private void WriteBody(BinaryWriter stream, IReadOnlyCollection<object> arr)
    {
        IWriter.WriteCompressedInt(stream, arr.Count);

        foreach (var o in arr)
        {
            if (o is string s)
            {
                stream.Write((byte) 0);
                stream.WriteCString(s);
            } 
            else if (o is float f)
            {
                stream.Write((byte) 1);
                stream.Write(f);
            }
            else if (o is int i)
            {
                stream.Write((byte) 2);
                stream.Write(i);
            }
            else if (o is List<object> lst)
            {
                stream.Write((byte) 3);
                WriteBody(stream, lst);
            }
            else
            {
                throw new Exception("Unknown Variable format");
            }
        }
    }
}
