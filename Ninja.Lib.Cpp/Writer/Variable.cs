﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class Variable: Entries.Variable, IWriter
{
    public void Write(BinaryWriter stream)
    {
        stream.Write((byte) 1);

        switch (Value)
        {
            case string:
                stream.Write((byte) 0);
                break;
            case float:
                stream.Write((byte) 1);
                break;
            case int i:
                stream.Write((byte) 2);
                break;
            default:
                throw new Exception("Unknown Variable format");
        }

        stream.WriteCString(Name);

        switch (Value)
        {
            case string str:
                stream.WriteCString(str);
                break;
            case float flt:
                stream.Write(flt);
                break;
            case int i:
                stream.Write(i);
                break;
        }
    }
}
