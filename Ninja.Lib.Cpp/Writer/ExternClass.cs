﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class ExternClass: Entries.ExternClass, IWriter
{
    public void Write(BinaryWriter stream)
    {
        stream.Write((byte) 3);
        stream.WriteCString(Name);
    }
}
