﻿namespace Ninja.Lib.Cpp.Writer;

public class RapFile: Entries.RapFile, IWriter
{
    public void Write(BinaryWriter stream)
    {
        stream.Write(ValidSignature);
        stream.Write((int) 0);
        stream.Write((int) 8);
        stream.Write((uint) 0); // offset to enums
        
        ((Class) EntryPoint).WriteBody(stream);


        var offsetToEnums = stream.BaseStream.Position;

        ((Enum)(Enum)).Write(stream);
        
        stream.Seek(12, SeekOrigin.Begin);
        stream.Write((uint) offsetToEnums);
    }
}
