﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Writer;

public class Class: Entries.Class, IWriter
{
    public void Write(BinaryWriter stream)
    {
        throw new NotImplementedException();
    }
    public void WriteBody(BinaryWriter stream)
    {
        stream.WriteCString(Inherited);
        IWriter.WriteCompressedInt(stream, Entries.Count);
        var offsetPoses = new List<long>();
        foreach (var entry in Entries)
        {
            if (entry is not Class c)
            {
                ((IWriter) entry).Write(stream);
                continue;
            }

            // write only header of class
            stream.Write((byte)0); // class type
            stream.WriteCString(c.Name);
            offsetPoses.Add(stream.BaseStream.Position);
            stream.Write((int)0); // offset to body
        }

        var endOfBodiesOffset = stream.BaseStream.Position;
        stream.Write((int)endOfBodiesOffset); // offset to the end of bodies

        var savedPos = stream.BaseStream.Position;
        if (offsetPoses.Count != 0)
        {
            // todo: ugly bunch of code
            // write body of classes
            var offsets = new List<uint>();
            foreach (var entry in Entries)
            {
                if (entry is not Class c) continue;

                offsets.Add((uint)stream.BaseStream.Position);
                c.WriteBody(stream);
            }

            // write offsets of bodies to the headers
            savedPos = stream.BaseStream.Position;
            for (var i = 0; i < offsetPoses.Count; i++)
            {
                stream.BaseStream.Seek(offsetPoses[i], SeekOrigin.Begin);
                stream.Write(offsets[i]);
            }
            stream.BaseStream.Seek(savedPos, SeekOrigin.Begin);
        }

        stream.BaseStream.Seek(endOfBodiesOffset, SeekOrigin.Begin);
        stream.Write((uint) savedPos);
        stream.BaseStream.Seek(savedPos, SeekOrigin.Begin);
    }
}
