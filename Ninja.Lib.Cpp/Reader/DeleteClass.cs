﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class DeleteClass : Entries.DeleteClass, IReader
{
    public void Read(BinaryReader stream)
    {
        Name = stream.ReadCString();
    }
}
