﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class Enum : Entries.Enum, IReader
{
    public void Read(BinaryReader stream)
    {
        var nEnums = stream.ReadUInt32();
        for (var i = 0; i < nEnums; i++)
        {
            var key = stream.ReadCString();
            var value = stream.ReadInt32();
            enumList.Add(key, value);
        }
    }
}
