﻿namespace Ninja.Lib.Cpp.Reader;

public class RapFile : Entries.RapFile, IReader
{
    public void Read(BinaryReader stream)
    {
        var signature = stream.ReadBytes(4);
        if (!signature.SequenceEqual(ValidSignature))
        {
            throw new InvalidOperationException("this is not a rap file");
        }

        stream.BaseStream.Seek(8, SeekOrigin.Current); // skip uint = 0 and uint = 8

        var offsetToEnums = stream.ReadUInt32();

        var entryPoint = new Class();
        entryPoint.ReadBody(stream);
        EntryPoint = entryPoint;

        stream.BaseStream.Seek(offsetToEnums, SeekOrigin.Begin);

        var e = new Enum();
        e.Read(stream);
        Enum = e;
    }
}
