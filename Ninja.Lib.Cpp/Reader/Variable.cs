﻿using Ninja.Lib.Cpp.Exceptions;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class Variable : Entries.Variable, IReader
{
    public void Read(BinaryReader stream)
    {
        var subtype = stream.ReadByte();
        Name = stream.ReadCString();
        if (subtype == 0)
        {
            Value = stream.ReadCString();
        }
        else if (subtype == 1)
        {
            Value = stream.ReadSingle();
        }
        else if (subtype == 2)
        {
            Value = stream.ReadInt32();
        }
        else
        {
            throw new DerapifyException("Unknown Variable format at position: " + stream.BaseStream.Position);
        }
    }
}
