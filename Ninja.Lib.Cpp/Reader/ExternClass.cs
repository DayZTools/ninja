﻿using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class ExternClass : Entries.ExternClass, IReader
{
    public void Read(BinaryReader stream)
    {
        Name = stream.ReadCString();
    }
}
