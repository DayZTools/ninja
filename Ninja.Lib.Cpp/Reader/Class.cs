﻿using Ninja.Lib.Cpp.Entries;
using Ninja.Lib.Cpp.Exceptions;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class Class : Entries.Class, IReader
{
    public void Read(BinaryReader stream)
    {
        Name = stream.ReadCString();
        var offsetToBody = stream.ReadUInt32();
        var savedOffset = stream.BaseStream.Position;

        stream.BaseStream.Seek(offsetToBody, SeekOrigin.Begin);

        ReadBody(stream);

        stream.BaseStream.Seek(savedOffset, SeekOrigin.Begin);
    }

    public void ReadBody(BinaryReader stream)
    {
        Inherited = stream.ReadCString();
        var nEntities = IReader.ReadCompressedInt(stream); // number of entries

        Entries = new List<Entry>();

        for (var i = 0; i < nEntities; ++i)
        {
            var type = stream.ReadByte();
            switch (type)
            {
                case 0:
                {
                    var e = new Class();
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                case 1:
                {
                    var e = new Variable();
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                case 2:
                {
                    var e = new Array { Append = false };
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                case 5:
                {
                    var e = new Array { Append = true };
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                case 3:
                {
                    var e = new ExternClass();
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                case 4:
                {
                    var e = new DeleteClass();
                    e.Read(stream);
                    Entries.Add(e);
                    break;
                }
                default:
                    throw new DerapifyException("Failed to parse Class body. Unknown entry type: "
                                                + (int)type
                                                + " at position: " + stream.BaseStream.Position);
            }
        }

        //stream.ReadUInt32(); // offset to the end of bodies
    }
}
