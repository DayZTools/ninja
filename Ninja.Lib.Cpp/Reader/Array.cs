﻿using Ninja.Lib.Cpp.Exceptions;
using Ninja.Lib.Utils;

namespace Ninja.Lib.Cpp.Reader;

public class Array : Entries.Array, IReader
{
    public void Read(BinaryReader stream)
    {
        Name = stream.ReadCString();
        Elements = ReadBody(stream);
    }

    private List<object> ReadBody(BinaryReader stream)
    {
        var arr = new List<object>();

        var nElems = IReader.ReadCompressedInt(stream);

        for (var i = 0; i < nElems; ++i)
        {
            var subtype = stream.ReadByte();
            if (subtype == 0)
            {
                arr.Add(stream.ReadCString());
            }
            else if (subtype == 1)
            {
                arr.Add(stream.ReadSingle());
            }
            else if (subtype == 2)
            {
                arr.Add(stream.ReadInt32());
            }
            else if (subtype == 3)
            {
                arr.Add(ReadBody(stream)); // recursion
            }
            else
            {
                throw new DerapifyException("Unknown Variable format at position: " + stream.BaseStream.Position);
            }
        }

        return arr;
    }
}
