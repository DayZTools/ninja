﻿namespace Ninja.Lib.Cpp.Reader;

public interface IReader
{
    protected static uint ReadCompressedInt(BinaryReader stream)
    {
        unchecked
        {
            ulong result = 0;

            for (var i = 0; i <= 4; i++)
            {
                var b = stream.ReadByte();
                result |= (uint)((b & 0x7f) << (i * 7));

                if (b < 0x80) break;
            }

            return (uint)result;
        }
    }

    void Read(BinaryReader stream);
}
