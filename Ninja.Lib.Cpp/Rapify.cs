﻿using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using Ninja.Lib.Cpp.Exceptions;
using Ninja.Lib.Cpp.Writer;
using Array = Ninja.Lib.Cpp.Writer.Array;

namespace Ninja.Lib.Cpp;

public static class Rapify
{
    public static RapFile CreateRapFile(Stream stream, bool isCpp = true)
    {
        var inputStream = new AntlrInputStream(stream);
        var lexer = new CppLexer(inputStream);
        var commonTokenStream = new CommonTokenStream(lexer);
        var parser = new CppParser(commonTokenStream);
        var parseTree = parser.translationUnit();
        var listener = new MyListener();
        var walker = new ParseTreeWalker();
        walker.Walk(listener, parseTree);

        var rf = listener.GetFile();

        if (isCpp)
        {
            var cfgPathes = rf.EntryPoint.Entries.FirstOrDefault(x => x.Name.Equals("CfgPatches"));

            if (cfgPathes is not Class c)
            {
                throw new RapifyException("Config must contain a CfgPathes");
            }

            if (c.Entries.FirstOrDefault(x => x is Class) is not Class addonClass)
            {
                throw new RapifyException("CfgPathes must contain subclass with addon name");
            }

            if (addonClass.Entries.FirstOrDefault(x => x.Name == "units") is not Array)
            {
                addonClass.Entries.Add(new Array { Name = "units" });
            }

            if (addonClass.Entries.FirstOrDefault(x => x.Name == "weapons") is not Array)
            {
                addonClass.Entries.Add(new Array { Name = "weapons" });
            }
        }

        return rf;
    }

    public static RapFile CreateRapFile(string filePath, bool isCpp = true)
    {
        var file = File.OpenRead(filePath);
        return CreateRapFile(file, isCpp);
    }
    
    public static void RapifyStream(Stream stream, BinaryWriter outStream, bool isCpp = true)
    {
        var rf = CreateRapFile(stream, isCpp);
        rf.Write(outStream);
    }

    public static void RapifyStream(Stream stream, Stream outStream, bool isCpp = true)
    {
        RapifyStream(stream, new BinaryWriter(outStream), isCpp);
    }

    public static void RapifyFile(string filePath, BinaryWriter outStream, bool isCpp = true)
    {
        var rf = CreateRapFile(filePath, isCpp);
        rf.Write(outStream);
    }

    public static void RapifyFile(string filePath, Stream outStream, bool isCpp = true)
    {
        RapifyFile(filePath, new BinaryWriter(outStream), isCpp);
    }
}
