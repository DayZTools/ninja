parser grammar CppParser;

@header {#pragma warning disable 3021}

options {
	tokenVocab = CppLexer;
}

translationUnit
    : statement* EOF
    ;

statement
    : classStatement
    | externStatement
    | deleteStatement
    | variableExpression
    | listExpression
    | appendListExpression
    ;

classStatement
    : Class Identifier extendClause? LBrace statement* RBrace Semicolon
    ;
    
extendClause
    : Colon Identifier
    ;

externStatement
    : Class Identifier Semicolon
    ;

deleteStatement
    : Delete Identifier Semicolon
    ;

variableExpression
    : Identifier (LBracket RBracket)? Assign primaryExpression Semicolon
    ;
    
listExpression
    : Identifier LBracket RBracket Assign initializerList Semicolon
    ;
    
appendListExpression
    :Identifier LBracket RBracket Plus Assign initializerList Semicolon
    ;

initializerList
    : LBrace (listValue (Coma listValue)*)? RBrace
    ;
    
listValue
    : primaryExpression | initializerList
    ;
    
primaryExpression
    : StringLiteral
    | IntegerLiteral
    | FloatingLiteral
    | Identifier
    ;
