﻿using System.Runtime.InteropServices;
using ImageMagick;
using Ninja.Lib.PAA;

DecodePAA(@"P:\Core\all\todo.paa", "test.png");

void DecodePAA(string inputPath, string outputPath)
{
    using var fs = File.OpenRead(inputPath);

    var paa = new Paa();
    paa.Read(fs);

    var mipLevel = 0;
    var data = paa.GetArgb32PixelData(fs, mipLevel);

    using var magic = new MagickImage(data, new PixelReadSettings(paa.Mipmaps[mipLevel].Width, paa.Mipmaps[mipLevel].Height, StorageType.Char, PixelMapping.RGBA));

    magic.Write(outputPath, MagickFormat.Png);
}

void EncodePAA(string inputPath, string outputPath)
{
    /*using var img = new MagickImage(inputPath);
    var px = img.GetPixels();
    var bytes = px.ToByteArray("ARGB");
    var pinnedArray = GCHandle.Alloc(bytes, GCHandleType.Pinned);
    var ptr = pinnedArray.AddrOfPinnedObject();
    var paa = new Paa();*/
}
