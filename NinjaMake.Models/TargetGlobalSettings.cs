﻿using System.ComponentModel;

namespace NinjaMake.Models;

/// <summary>
/// Global settings for target
/// </summary>
public class TargetGlobalSettings : INotifyPropertyChanged
{
    /// <summary>
    /// Used for all targets that does not declared BuildUnit.SourcePath
    /// </summary>
    public string? SourceRootPath { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.OutputPath
    /// </summary>
    public string? OutputPath { get; set; } = "";

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.StagePath
    /// </summary>
    public string? StagePath { get; set; } = null;

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.PrivateKey
    /// </summary>
    public string? PrivateKey { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.Sign
    /// </summary>
    public bool? Sign { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.CopyKey
    /// </summary>
    public bool? CopyKey { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.CompressList
    /// </summary>
    public string[]? CompressList { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.ExclusionList
    /// </summary>
    public string[]? ExclusionList { get; set; }

    /// <summary>
    /// Used for all targets that does not declared BuildUnit.BuildScripts
    /// </summary>
    public List<BuildScript>? BuildScripts { get; set; }
    
    /// <summary>
    /// Used to auto set Prefix for PBO
    /// </summary>
    public string? PrefixRoot { get; set; }

    #region INotifyPropertyChanged Members
    
    /// \internal
    public event PropertyChangedEventHandler? PropertyChanged;

    private void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
}
