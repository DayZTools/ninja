﻿using System.ComponentModel;

namespace NinjaMake.Models;

/// <summary>
/// Makefile configuration
/// </summary>
public class Config : INotifyPropertyChanged
{
    /// <summary>
    /// Default settings applied to targets
    /// </summary>
    public DefaultSettings? Default { get; set; }
    
    /// <summary>
    /// Build targets
    /// </summary>
    public BuildUnit[]? Targets { get; set; }

    #region INotifyPropertyChanged Members
    
    /// \internal
    public event PropertyChangedEventHandler? PropertyChanged;

    private void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
}
