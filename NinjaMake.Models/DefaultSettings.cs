﻿using System.ComponentModel;

namespace NinjaMake.Models;

/// <summary>
/// Default settings
/// </summary>
public class DefaultSettings : INotifyPropertyChanged
{
    /// <summary>
    /// Used for all targets that does not declared TargetGlobalSettings.SourceRootPath or BuildUnit.SourcePath
    /// </summary>
    public string? SourceRootPath { get; set; }
    
    /// <summary>
    /// Used for all targets that does not declared TargetGlobalSettings.StagePath or BuildUnit.StagePath
    /// </summary>
    public string? StagePath { get; set; } = null;
    
    /// <summary>
    /// Used for all targets that does not declared TargetGlobalSettings.PrivateKey or BuildUnit.PrivateKey
    /// </summary>
    public string? PrivateKey { get; set; }
    
    /// <summary>
    /// Global target settings
    /// </summary>
    public Dictionary<string, TargetGlobalSettings>? Targets { get; set; }
    
    /// <summary>
    /// Used to auto set Prefix for PBO
    /// </summary>
    public string? PrefixRoot { get; set; }

    #region INotifyPropertyChanged Members
    
    /// \internal
    public event PropertyChangedEventHandler? PropertyChanged;

    private void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
}
