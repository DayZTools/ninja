﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace NinjaMake.Models;

/// <summary>
/// Target settings
/// </summary>
public class BuildUnit : INotifyPropertyChanged
{
    /// <summary>
    /// Name of current BuildUnit
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Name of TargetGlobalSettings
    /// </summary>
    public string? Type { get; set; }

    /// <summary>
    /// Path to source code (config.cpp location)
    /// </summary>
    public string? SourcePath { get; set; }

    /// <summary>
    /// Output for PBO files
    /// </summary>
    public string? OutputPath { get; set; }

    /// <summary>
    /// Path to intermediate files
    /// </summary>
    public string? StagePath { get; set; }

    /// <summary>
    /// Path to the private key
    /// </summary>
    public string? PrivateKey { get; set; }

    /// <summary>
    /// Name of resulting pbo
    /// </summary>
    public string? PboName { get; set; }

    /// <summary>
    /// Create bisign for pbo
    /// </summary>
    public bool? Sign { get; set; }

    /// <summary>
    /// Copy bikey to output/keys
    /// </summary>
    public bool? CopyKey { get; set; }

    /// <summary>
    /// List of patterns or files to compress
    /// </summary>
    public string[]? CompressList { get; set; }

    /// <summary>
    /// List of patterns or files to skip from packing to PBO
    /// </summary>
    public string[]? ExclusionList { get; set; }

    /// <summary>
    /// Prefix in PBO
    /// </summary>
    public string? Prefix
    {
        get => Properties?.Where(x => x.Key == "prefix").Select(x => x.Value).FirstOrDefault();
        set
        {
            if (Properties == null) return;
            if (value != null)
                Properties.Add("prefix", value);
            else
                Properties.Remove("prefix");
        }
    }

    /// <summary>
    /// PBO Properties
    /// </summary>
    public Dictionary<string, string>? Properties { get; set; }

    /// <summary>
    /// Build scripts
    /// </summary>
    public List<BuildScript>? BuildScripts { get; set; }
    
    /// <summary>
    /// Used to auto set Prefix for PBO
    /// </summary>
    public string? PrefixRoot { get; set; }

    [JsonIgnore]
    public List<string> GeneratedFiles { get; set; } = new();

    #region INotifyPropertyChanged Members

    /// \internal
    public event PropertyChangedEventHandler? PropertyChanged;

    private void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
}
