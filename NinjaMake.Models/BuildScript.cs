﻿namespace NinjaMake.Models;

/// <summary>
/// Type of BuildScript hook
/// </summary>
public enum BuildScriptType
{
    /// <summary>
    /// called right after initializing Target
    /// </summary>
    BeforeBuild,
    /// <summary>
    /// called after all build steps but before pack
    /// </summary>
    BeforePack,
    /// <summary>
    /// fired after all build steps
    /// </summary>
    AfterBuild
}

/// <summary>
/// Script settings
/// </summary>
public class BuildScript
{
    /// <summary>
    /// Type of hook
    /// </summary>
    public BuildScriptType Type { get; set; }

    /// <summary>
    /// Path to the PowerShell script
    /// </summary>
    public string Path { get; set; }
}
