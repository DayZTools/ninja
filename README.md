# NinjaTools

Is a set of libraries and utilities written on C# designed to help modding DayZ

## Content

### Utilities

* [NinjaMake](NinjaMake/README.md) a tool to ease PBO build process
* [NinjaKeyGen]() a tool to generate BiPrivate and BiPublic keys

### Libraries

* [Ninja.Lib.Cpp](Ninja.Lib.Cpp) a library to Rapify and DeRapify ``*.cpp`` and ``*.rvmat`` files
* [Ninja.Lib.DSKeys](Ninja.Lib.DSKeys) a library to operate with BIS keys and signatures
* [Ninja.Lib.Lzss](Ninja.Lib.Lzss) a library to work with LZSS (Lempel–Ziv–Storer–Szymanski) compression
* [Ninja.Lib.P3D](Ninja.Lib.P3D) a library to binarize BIS models
* [Ninja.Lib.PAA](Ninja.Lib.PAA) a library to manipulate images
* [Ninja.Lib.Pbo](Ninja.Lib.Pbo) a library to pack and unpack BIS archives
* [Ninja.Lib.Utils](Ninja.Lib.Utils) a helper library
