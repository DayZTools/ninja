using System.Text;

namespace Ninja.Lib.Utils;

public sealed class MemoryIndendedTextWriter : IndendedTextWriter, IDisposable
{
    private MemoryStream _stream = new MemoryStream();

    public MemoryIndendedTextWriter()
    {
        OutStream = new StreamWriter(_stream);
        OutStream.AutoFlush = true;
    }

    public void Dispose()
    {
        _stream.Dispose();
    }

    public override string ToString()
    {
        return Encoding.UTF8.GetString(_stream.ToArray());
    }
}
