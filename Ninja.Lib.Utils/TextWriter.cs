﻿namespace Ninja.Lib.Utils;

public class IndendedTextWriter: IDisposable
{
    protected StreamWriter? OutStream { get; init; }
    private int _ident = 0;

    protected IndendedTextWriter()
    {
    }

    public IndendedTextWriter(StreamWriter outStream)
    {
        OutStream = outStream;
        OutStream.AutoFlush = true;
    }

    public IndendedTextWriter(Stream outStream)
    {
        OutStream = new StreamWriter(outStream);
        OutStream.AutoFlush = true;
    }

    public void UpIdent()
    {
        _ident += 1;
    }

    public void DownIdent()
    {
        _ident -= 1;
    }

    public void WriteLine() => OutStream?.WriteLine();

    public void WriteLine(string str) => OutStream?.WriteLine(str);

    public void Write(string str) => OutStream?.Write(str);

    public void WriteIndendedLine(string str)
    {
        WriteIndent();
        WriteLine(str);
    }

    public void WriteIndended(string str)
    {
        WriteIndent();
        Write(str);
    }

    public void WriteIndent() => OutStream?.Write(new string(' ', _ident * 4));

    public void Dispose()
    {
        OutStream?.Dispose();
    }
}
