﻿namespace Ninja.Lib.Utils;

public static class DirectoryExt
{
    public static string CreateTemporaryDirectory()
    {
        while (true)
        {
            var tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            if (Directory.Exists(tempDirectory)) continue;
            Directory.CreateDirectory(tempDirectory);
            return tempDirectory;
        }
    }
}
