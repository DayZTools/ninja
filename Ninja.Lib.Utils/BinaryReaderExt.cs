﻿using System.Text;

namespace Ninja.Lib.Utils;

public static class BinaryReaderExt
{
    /// <summary>
    /// Keeps reading bytes until it encounters 0 then returns as string
    /// </summary>
    /// <returns></returns>
    public static string ReadCString(this BinaryReader br)
    {
        var bytes = new List<byte>();

        // Run through bytes from current index until we encounter 0
        for (var i = br.ReadByte(); i != 0; i = br.ReadByte())
            bytes.Add(i);

        return Encoding.ASCII.GetString(bytes.ToArray());
    }

    public static string ReadRawString(this BinaryReader br, int size)
    {
        var bytes = br.ReadBytes(size);
        var i = 0;
        for (; i < bytes.Length; ++i)
        {
            if (bytes[i] == '\0') break;
        }

        return i > 0 ? Encoding.ASCII.GetString(bytes[..i]) : string.Empty;
    }

    public static void Peek(this BinaryReader br, byte[] buf, int offset, int size)
    {
        var pos = br.BaseStream.Position;
        br.Read(buf, offset, size);
        br.BaseStream.Seek(pos, SeekOrigin.Begin);
    }

    public static void Peek(this BinaryReader br, out byte b)
    {
        b = br.ReadByte();
        br.BaseStream.Seek(-1, SeekOrigin.Current);
    }

    public static int PeekInt(this BinaryReader br)
    {
        var v = br.ReadInt32();
        br.BaseStream.Seek(-4, SeekOrigin.Current);
        return v;
    }

    public static int ReadInt24(this BinaryReader br)
    {
        return (int)(br.ReadByte() + (br.ReadByte() << 8) + (br.ReadByte() << 16));
    }
}
