﻿using System.Text;

namespace Ninja.Lib.Utils;

public static class BinaryWriterExt
{
    public static void WriteCString(this BinaryWriter bw, string buffer)
    {
        var x = Encoding.UTF8.GetBytes(buffer);
        bw.Write(x);
        bw.Write((byte)0);
    }

    public static void WriteRawString(this BinaryWriter bw, string buffer, int len = -1)
    {
        var x = Encoding.UTF8.GetBytes(buffer);
        bw.Write(x);
        if (len != -1 && len - buffer.Length > 0)
        {
            bw.Write(new string('\0', len - buffer.Length));
        }
    }

    public static void WriteInt24(this BinaryWriter bw, int value)
    {
        var val = BitConverter.GetBytes(value);
        bw.Write(val[0]);
        bw.Write(val[1]);
        bw.Write(val[2]);
    }
}
