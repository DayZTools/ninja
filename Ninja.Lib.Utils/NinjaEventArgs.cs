﻿namespace Ninja.Lib.Utils;

public enum EventType
{
    Info = 1,
    Warning = 2,
    Error = 3,
    Debug = 4,
}


public delegate void NinjaEventHandler(NinjaEventArgs args);

public class NinjaEventArgs : EventArgs
{
    public string Message { get; }
    public EventType Type { get; }

    public NinjaEventArgs(string message, EventType type)
    {
        Message = message;
        Type = type;
    }
}
